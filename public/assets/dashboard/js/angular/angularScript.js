/**
 * Created by psybo-03 on 12/12/17.
 */

var app = angular.module('nims', ['ngRoute', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngFileUpload', 'cp.ngConfirm', 'angular-loading-bar']);
app.config(['$routeProvider', '$locationProvider','cfpLoadingBarProvider', function ($routeProvider, $locationProvider,cfpLoadingBarProvider) {
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            templateUrl: 'dashboard/dashboard'
        })
        .when('/dashboard', {
            templateUrl: 'dashboard/dashboard',
            controller: 'DashboardController'
        })
        .when('/slider', {
            templateUrl: 'dashboard/slider',
            controller: 'SlideController'
        })
        .when('/testimonial', {
            templateUrl: 'dashboard/testimonial',
            controller: 'TestimonialController'
        })
       .when('/services', {
            templateUrl: 'dashboard/service',
            controller: 'ServiceController'
        })
        .when('/edit-user',{
            templateUrl: 'edit-user'
        })
        .when('/users',{
            templateUrl: 'users'
        })
        .when('/team', {
            templateUrl: 'dashboard/team',
            controller: 'TeamController'
        })
        .when('/news', {
            templateUrl: 'dashboard/news',
            controller: 'NewsController'
        })
        .when('/events', {
            templateUrl: 'dashboard/events',
            controller: 'EventsController'
        })

}]);

//Pagination filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

