/**
 * Created by psybo-03 on 12/12/17.
 */

var app = angular.module('nims', ['ngRoute', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngFileUpload', 'cp.ngConfirm', 'angular-loading-bar']);
app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider', function ($routeProvider, $locationProvider, cfpLoadingBarProvider) {
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            templateUrl: 'dashboard/dashboard'
        })
        .when('/dashboard', {
            templateUrl: 'dashboard/dashboard',
            controller: 'DashboardController'
        })
        .when('/media', {
            templateUrl: 'dashboard/media',
            controller: 'MediaController'
        })
        .when('/news', {
            templateUrl: 'dashboard/news',
            controller: 'NewsController'
        })
        .when('/testimonial', {
            templateUrl: 'dashboard/testimonial',
            controller: 'TestimonialController'
        })
       .when('/team', {
            templateUrl: 'dashboard/team',
            controller: 'TeamController'
        })
        .when('/events', {
            templateUrl: 'dashboard/events',
            controller: 'EventsController'
        })
        .when('/documents', {
            templateUrl: 'dashboard/documents',
            controller: 'DocumentController'
        })
        .when('/settings', {
            templateUrl: 'dashboard/settings',
            controller: 'SettingsController'
        })
}]);

//Pagination filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});


/**
 * Created by psybo-03 on 09/09/17.
 */

app.controller('AdminController', ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', '$ngConfirm', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, $ngConfirm) {

    $scope.error = {};
    $rootScope.base_url = $location.protocol() + "://" + location.host + '/';
    $rootScope.public_url = $location.protocol() + "://" + location.host + '/public/';

    $scope.currentPage = 1;
    $scope.paginations = [5, 10, 20, 25];
    $scope.numPerPage = 10;

    $scope.user = {};
    $scope.curuser = {};
    $scope.newuser = {};
    $scope.newuser = {};
    $scope.formdisable = false;
    $scope.edituser = false;
    $rootScope.url = 'dashboard';


    $scope.format = 'yyyy/MM/dd';

    $scope.validationError = {};

    //$scope.date = new Date();

    //check_thumb();

    load_user();


    function load_user() {
        var url = $rootScope.base_url + 'dashboard/load-user';
        $http.get(url).then(function (response) {
            if (response.data) {
                $scope.curuser.username = response.data.username;
                $scope.curuser.id = response.data.id;
            }
        });
    }

    function check_thumb() {
        var url = $rootScope.base_url + 'admin/check-thumb';
        $http.post(url, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined, 'Process-Data': false}
        })
            .then(function nSuccess(response) {
                console.log('success');
            }, function onError(response) {
                console.log('error');
            })
    }

    $scope.editProfile = function (id) {
        var userid = angular.element(document.getElementsByName('userid')[0]).val();
        var fd = new FormData();

        angular.forEach($scope.curuser, function (item, key) {
            fd.append(key, item);
        });
        var url = $rootScope.base_url + 'dashboard/edit-user/' + id;
        $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined, 'Process-Data': false}
        })
            .then(function onSuccess(response) {
                $scope.curuser = {};
                load_user();
                $ngConfirm({
                    title: 'Alert!',
                    content: '<strong>Updated!</strong>',
                    buttons: {
                        close: function(scope, button){
                            $window.location.href = '/dashboard#';
                        }
                    }
                });
            }, function onError(response) {
                $scope.validationError = response.data;
            });
    };

    $scope.cancel = function () {
        $window.location.href = '/#';
    };


    /******DATE Picker start******/
    $scope.today = function () {
        $scope.date = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.date = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.date = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }


    /******DATE Picker END******/

    function openModal(content, size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            controllerAs: '$scope',
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return content;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }


}]);

/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('DocumentController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.documents = [];
        $scope.newdocument = {};
        $scope.curdocument = false;
        $scope.files = false;
        $scope.errFiles = false
        ;
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadDocument();

        function loadDocument() {
            $http.get($rootScope.base_url + 'dashboard/document/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.documents = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newDocument = function () {
            $scope.newdocument = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = false
            ;
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editDocument = function (item) {
            $scope.showform = true;
            $scope.curdocument = item;
            $scope.newdocument = angular.copy(item);
            $scope.files = false;
        };

        $scope.hideForm = function () {
            $scope.errFiles = false
            ;
            $scope.showform = false;
        };

        $scope.addDocument = function () {

            var fd = new FormData();

            angular.forEach($scope.newdocument, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newdocument['id']) {
                var url = $rootScope.base_url + 'dashboard/document/edit/' + $scope.newdocument.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadDocument();
                        $scope.newdocument = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if ($scope.uploaded != null) {
                    var url = $rootScope.base_url + 'dashboard/document/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadDocument();
                            $scope.newdocument = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteDocument = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/document/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.documents.indexOf(item);
                                    $scope.documents.splice(index, 1);
                                    loadDocument();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles = errFile;
            });
            console.log($scope.errFiles);
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/document/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/document/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showDocumentFiles = function (item) {
            console.log(item);
            $scope.documentfiles = item;
        };
    }]);


/**
 * Created by psybo-03 on 1/1/18.
 */

app.controller('EventsController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.eventss = [];
        $scope.newevents = {};
        $scope.curevents = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadEvents();

        function loadEvents() {
            $http.get($rootScope.base_url + 'dashboard/events/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.eventss = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newEvents = function () {
            $scope.newevents = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editEvents = function (item) {
            $scope.showform = true;
            $scope.curevents = item;
            $scope.newevents = angular.copy(item);
            $scope.files = false;
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addEvents = function () {

            var fd = new FormData();

            angular.forEach($scope.newevents, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newevents['id']) {
                var url = $rootScope.base_url + 'dashboard/events/edit/' + $scope.newevents.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadEvents();
                        $scope.newevents = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if ($scope.uploaded != null) {
                    var url = $rootScope.base_url + 'dashboard/events/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadEvents();
                            $scope.newevents = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteEvents = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/events/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.eventss.indexOf(item);
                                    $scope.eventss.splice(index, 1);
                                    loadEvents();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/events/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/events/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showEventsFiles = function (item) {
            console.log(item);
            $scope.eventsfiles = item;
        };


        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (events, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return events;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);



/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('MediaController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.medias = [];
        $scope.newmedia = {};
        $scope.curmedia = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadMedia();

        function loadMedia() {
            $http.get($rootScope.base_url + 'dashboard/media/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.medias = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newMedia = function () {
            $scope.newmedia = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editMedia = function (item) {
            $scope.showform = true;
            $scope.curmedia = item;
            $scope.newmedia = angular.copy(item);
            $scope.files = false;
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addMedia = function () {

            var fd = new FormData();

            angular.forEach($scope.newmedia, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newmedia['id']) {
                var url = $rootScope.base_url + 'dashboard/media/edit/' + $scope.newmedia.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadMedia();
                        $scope.newmedia = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if ($scope.uploaded != null) {
                    var url = $rootScope.base_url + 'dashboard/media/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadMedia();
                            $scope.newmedia = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteMedia = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/media/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.medias.indexOf(item);
                                    $scope.medias.splice(index, 1);
                                    loadMedia();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/media/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/media/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showMediaFiles = function (item) {
            console.log(item);
            $scope.mediafiles = item;
        };


        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (media, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return media;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);


/**
 * Created by psybo-03 on 6/1/18.
 */


app.controller('NewsController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {
        
    $scope.newss = [];
    $scope.newnews = {};
    $scope.curnews = {};
    $scope.errFiles = [];
    $scope.showform = false;
    $scope.message = {};
    $rootScope.url = $location.path().replace('/', '');
    $scope.uploaded = [];
    // $scope.fileValidation = {};


    loadNews();

    function loadNews() {
        $http.get($rootScope.base_url + 'dashboard/news/get').then(function (response) {
            if (response.data) {
                $scope.newss = response.data;
                $scope.showtable = true;
            } else {
                console.log('No data Found');
                $scope.showtable = false;
                $scope.message = 'No data found';
            }
        });
    }


    $scope.newNews = function () {
        $scope.newnews = {};
        $scope.filespre = [];
        $scope.uploaded = [];
        $scope.files = [];
        $scope.errFiles = [];
        $scope.showform = true;
        $scope.item_files = false;
        $scope.curnews = false;
    };

    $scope.editNews = function (item) {
        $scope.files = [];
        $scope.uploaded = [];
        $scope.showform = true;
        $scope.curnews = item;
        $scope.newnews = angular.copy(item);
        $scope.item_files = item.files;
    };

    $scope.hideForm = function () {
        $scope.errFiles = '';
        $scope.showform = false;
    };

    $scope.addNews = function () {

        var fd = new FormData();
        angular.forEach($scope.newnews, function (item, key) {
            fd.append(key, item);
        });

        fd.append('uploaded', JSON.stringify($scope.uploaded));

        if ($scope.newnews['id']) {
            var url = $rootScope.base_url + 'dashboard/news/edit/' + $scope.newnews.id;
            $http.post(url, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined, 'Process-Data': false}
            })
                .then(function onSuccess(response) {
                    $scope.newss.push(response.data);
                    loadNews();
                    $scope.newnews = {};
                    $scope.showform = false;
                    $scope.files = '';
                }, function onError(response) {
                    console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                    $scope.files = '';
                });
        } else {
            var url = $rootScope.base_url + 'dashboard/news/add';

            $http.post(url, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined, 'Process-Data': false}
            })
                .then(function onSuccess(response) {
                    $scope.newss.push(response.data);
                    loadNews();
                    $scope.newnews = {};
                    $scope.showform = false;
                    $scope.files = [];

                }, function onError(response) {
                    console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                    $scope.validationError = response.data;
                });
        }
    };

    $scope.deleteNews = function (item) {
          $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
        var url = $rootScope.base_url + 'dashboard/news/delete/' + item['id'];
        $http.delete(url)
            .then(function onSuccess(response) {
                var index = $scope.newss.indexOf(item);
                $scope.newss.splice(index, 1);
                alert(response.data.msg);
                loadNews();
            },function onError(response) {
                console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                console.log(response.data);
            });
             }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });
    };


    $scope.uploadFiles = function (files, errFiles) {
        angular.forEach(errFiles, function (errFile) {
            $scope.errFiles.push(errFile);
        });
        angular.forEach(files, function (file) {
            $scope.files.push(file);
            file.upload = Upload.upload({
                url: $rootScope.base_url + 'dashboard/news/upload',
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    $scope.uploaded.push(response.data);
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                evt.loaded / evt.total));
            });
        });
    };

    $scope.deleteImage =function(item) {
        var url = $rootScope.base_url + 'dashboard/news/delete-image/' + item['id'];
        $http.delete(url)
            .then(function onSuccess(response) {
                console.log('image deleted');
                /*remove deleted file from scope variable*/
                var index = $scope.item_files.indexOf(item);
                $scope.item_files.splice(index, 1);
            },function onError(response) {
                console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                console.log(response.data);
            });
    };

    $scope.showNewsFiles = function (item) {
        console.log(item);
        $scope.newsfiles = item;
    };



    /****Modal***/

    $scope.animationsEnabled = true;

    $scope.showNews = function (news,size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'dashboard/viewNews',
            controller: 'ModalInstanceCtrl',
            controllerAs: '$scope',
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return news;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    /****Modal end***/

    $scope.removeImage = function (image) {
        angular.forEach($scope.uploaded,function(item,key) {
            if (item.client_name == image.name) {
                var index = $scope.files.indexOf(image);
                $scope.files.splice(index, 1);
                var index = $scope.uploaded.indexOf(item);
                $scope.uploaded.splice(index, 1);
                console.log($scope.uploaded);
                console.log($scope.files);
            }
        })

    };


}]);






/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('SettingsController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.setings = [];
        $scope.newdocument = {};
        $scope.curdocument = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadDocument();

        function loadDocument() {
            $http.get($rootScope.base_url + 'dashboard/document/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.setings = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newDocument = function () {
            $scope.newdocument = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editDocument = function (item) {
            $scope.showform = true;
            $scope.curdocument = item;
            $scope.newdocument = angular.copy(item);
            $scope.files = false;
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addDocument = function () {

            var fd = new FormData();

            angular.forEach($scope.newdocument, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newdocument['id']) {
                var url = $rootScope.base_url + 'dashboard/document/edit/' + $scope.newdocument.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadDocument();
                        $scope.newdocument = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if ($scope.uploaded != null) {
                    var url = $rootScope.base_url + 'dashboard/document/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadDocument();
                            $scope.newdocument = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteDocument = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/document/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.setings.indexOf(item);
                                    $scope.setings.splice(index, 1);
                                    loadDocument();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/document/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/document/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showDocumentFiles = function (item) {
            console.log(item);
            $scope.documentfiles = item;
        };


        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (document, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return document;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);


/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('TeamController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.teams = [];
        $scope.newteam = {};
        $scope.curteam = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadTeam();

        function loadTeam() {
            $http.get($rootScope.base_url + 'dashboard/team/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.teams = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newTeam = function () {
            $scope.newteam = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editTeam = function (item) {
            $scope.showform = true;
            $scope.curteam = item;
            $scope.newteam = angular.copy(item);
            $scope.files = false;
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addTeam = function () {

            var fd = new FormData();

            angular.forEach($scope.newteam, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newteam['id']) {
                var url = $rootScope.base_url + 'dashboard/team/edit/' + $scope.newteam.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadTeam();
                        $scope.newteam = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if ($scope.uploaded != null) {
                    var url = $rootScope.base_url + 'dashboard/team/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadTeam();
                            $scope.newteam = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteTeam = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                           var url = $rootScope.base_url + 'dashboard/team/delete/' + item['id'];
                                            $http.delete(url)
                                                .then(function onSuccess(response) {
                                                    var index = $scope.teams.indexOf(item);
                                                    $scope.teams.splice(index, 1);
                                                    loadTeam();
                                                }, function onError(response) {
                                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                                    console.log(response.data);
                                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });
        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/team/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/team/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showTeamFiles = function (item) {
            console.log(item);
            $scope.teamfiles = item;
        };


        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (team, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return team;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);


/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('TestimonialController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.testimonials = [];
        $scope.newtestimonial = {};
        $scope.curtestimonial = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadTestimonial();

        function loadTestimonial() {
            $http.get($rootScope.base_url + 'dashboard/testimonial/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.testimonials = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newTestimonial = function () {
            $scope.newtestimonial = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editTestimonial = function (item) {
            $scope.showform = true;
            $scope.curtestimonial = item;
            $scope.newtestimonial = angular.copy(item);
            $scope.files = false;
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addTestimonial = function () {

            var fd = new FormData();

            angular.forEach($scope.newtestimonial, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newtestimonial['id']) {
                var url = $rootScope.base_url + 'dashboard/testimonial/edit/' + $scope.newtestimonial.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadTestimonial();
                        $scope.newtestimonial = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if ($scope.uploaded != null) {
                    var url = $rootScope.base_url + 'dashboard/testimonial/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadTestimonial();
                            $scope.newtestimonial = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteTestimonial = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/testimonial/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.testimonials.indexOf(item);
                                    $scope.testimonials.splice(index, 1);
                                    loadTestimonial();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/testimonial/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/testimonial/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showTestimonialFiles = function (item) {
            console.log(item);
            $scope.testimonialfiles = item;
        };


        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (testimonial, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return testimonial;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);


/**
 * Created by psybo-03 on 24/10/17.
 */
app.controller('ModalInstanceCtrl', function ($uibModalInstance, items,$scope) {
    $scope.items = items;
    console.log(items);
    $scope.ok = function () {
        $uibModalInstance.close($scope.items);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
