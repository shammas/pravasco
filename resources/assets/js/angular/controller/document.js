/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('DocumentController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.documents = [];
        $scope.newdocument = {};
        $scope.curdocument = false;
        $scope.files = false;
        $scope.errFiles = false
        ;
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadDocument();

        function loadDocument() {
            $http.get($rootScope.base_url + 'dashboard/document/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.documents = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newDocument = function () {
            $scope.newdocument = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = false
            ;
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editDocument = function (item) {
            $scope.showform = true;
            $scope.curdocument = item;
            $scope.newdocument = angular.copy(item);
            $scope.files = false;
        };

        $scope.hideForm = function () {
            $scope.errFiles = false
            ;
            $scope.showform = false;
        };

        $scope.addDocument = function () {

            var fd = new FormData();

            angular.forEach($scope.newdocument, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newdocument['id']) {
                var url = $rootScope.base_url + 'dashboard/document/edit/' + $scope.newdocument.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadDocument();
                        $scope.newdocument = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if ($scope.uploaded != null) {
                    var url = $rootScope.base_url + 'dashboard/document/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadDocument();
                            $scope.newdocument = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteDocument = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/document/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.documents.indexOf(item);
                                    $scope.documents.splice(index, 1);
                                    loadDocument();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles = errFile;
            });
            console.log($scope.errFiles);
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/document/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/document/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showDocumentFiles = function (item) {
            console.log(item);
            $scope.documentfiles = item;
        };
    }]);

