<?php


class News_file_model extends MY_Model
{
    public $table = 'news_files';
    function __construct()
    {
        $this->has_one['file'] = ['foreign_model' => 'File_model', 'foreign_table' => 'files', 'foreign_key' => 'id', 'local_key' => 'file_id'];
        parent::__construct();
        $this->timestamps = FALSE;

    }

    /**
     *return all news files one by one
     *
     */
    public function select_files($limit = null, $order = null)
    {
        $this->db->from('news');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($order != null) {
            $this->db->order_by($order, 'DESC');
        }
        $all_files = [];
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $newss = $query->result();
            foreach ($newss as $value) {
                $this->db->from('news_files');
                $this->db->where('news_id', $value->id);
                $glr_fls_query = $this->db->get();
                $news_files = $glr_fls_query->result();
                foreach ($news_files as $val) {
                    $this->db->from('files');
                    $this->db->where('id', $val->file_id);
                    $files_query = $this->db->get();
                    $files = $files_query->result();
                    foreach ($files as $file) {
                        $val->thumbUrl = public_url() . 'uploads/thumb/thumb_' . $file->file_name;
                        $val->url = base_url() . 'uploads/' . $file->file_name;
                        $val->alt = 'news';
                        $val->news_name = $value->name;
                        $val->description = $value->description;
                        array_push($all_files, $val);
                    }
                }
            }
            return $all_files;
        }else
            return FALSE;
    }

}