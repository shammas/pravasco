<?php
/**
 * Gallery_model.php
 * User: Noushid P
 * Date: 6/1/18
 * Time: 1:00 PM
 */

defined('BASEPATH') or exit('No direct Script access allowed');
class News_model extends MY_Model
{

    function __construct()
    {

        $this->has_many_pivot['files'] = array(
            'foreign_model'=>'file_model',
            'pivot_table'=>'news_files',
            'local_key'=>'id',
            'pivot_local_key'=>'news_id', /* this is the related key in the pivot table to the local key
		        this is an optional key, but if your column name inside the pivot table
		        doesn't respect the format of "singularlocaltable_primarykey", then you must set it. In the next title
		        you will see how a pivot table should be set, if you want to  skip these keys */
            'pivot_foreign_key'=>'file_id', /* this is also optional, the same as above, but for foreign table's keys */
            'foreign_key'=>'id',
            'get_relate'=>TRUE /* another optional setting, which is explained below */
        );


        parent::__construct();
        $this->timestamps = TRUE;
    }



}