<?php
/**
 * Blog_model.php
 * User: Noushid P
 * Date: 1/1/18
 * Time: 3:35 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Events_model extends MY_Model
{

    function __construct()
    {
        /*Third array value is updated value for current page*/
        $this->pagination_delimiters = array('<li>','</li>','<li class="active">');
        $this->pagination_arrows = array('previous','next');

        $this->has_one['file'] = array('foreign_model' => 'File_model', 'foreign_table' => 'files', 'foreign_key' => 'id', 'local_key' => 'file_id');
        parent::__construct();
        $this->timestamps = TRUE;
    }

}