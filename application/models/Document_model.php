<?php
/**
 * Testimonial_model.php
 * User: Noushid P
 * Date: 1/1/18
 * Time: 5:15 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Document_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
        $this->table = 'documents';
    }

}