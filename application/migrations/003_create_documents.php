<?php
/**
 * 002_initial_schema.php
 * User: Noushid P
 * Date: 13/12/17
 * Time: 12:08 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_documents extends CI_Migration {

    public function up()
    {
        /**
         * Table structure for table 'documents'
         *
         */

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'file_type' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'path' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('documents');

    }

    public function down()
    {
        $this->dbforge->drop_table('documents', TRUE);
    }
}