<?php
/**
 * Media_Controller.php
 * User: Noushid P
 * Date: 1/1/18
 * Time: 5:13 PM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Media_Controller extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Media_model', 'jjmedia');
        $this->load->model('File_model', 'file');

        $this->load->library(['upload', 'image_lib','ion_auth']);

        $this->load->library('form_validation');
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }
    }

    function index()
    {
        $data = $this->jjmedia->with_file()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    function get_all()
    {
        $data = $this->jjmedia->with_file()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);

            if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                $file_data['file_name'] = $uploaded->file_name;
                $file_data['file_type'] = $uploaded->file_type;
                $file_data['size'] = $uploaded->file_size;
                $file_data['url'] = base_url() . 'uploads/';
                $file_data['path'] = getwdir() . 'uploads/';
                $file_id = $this->file->insert($file_data);

                $post_data['file_id'] = $file_id;

                $jjmedia_id = $this->jjmedia->insert($post_data);

                if ($jjmedia_id) {
                    if (!is_dir(getwdir().'uploads/thumb')) {
                        mkdir(getwdir() . 'uploads/thumb', 0777, TRUE);
                    }

                    /*****Create Thumb Image****/
                    $img_cfg['source_image'] = getwdir() . 'uploads/' . $uploaded->file_name;
                    $img_cfg['maintain_ratio'] = TRUE;
                    $img_cfg['new_image'] = getwdir() . 'uploads/thumb/' . $uploaded->file_name;
                    $img_cfg['quality'] = 99;
                    $img_cfg['master_dim'] = 'height';
                    $img_cfg['height'] = 50;

                    $resize_error = [];
                    $this->image_lib->initialize($img_cfg);
                    if (!$this->image_lib->resize()) {
                        $resize_error[] = $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();

                    /********End Thumb*********/

                    /*resize and create thumbnail image*/
                    if ($uploaded->file_size > 1024) {
                        $img_cfg['image_library'] = 'gd2';
                        $img_cfg['source_image'] = getwdir() . 'uploads/' . $uploaded->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getwdir() . 'uploads/' . $uploaded->file_name;
                        $img_cfg['height'] = 500;
                        $img_cfg['quality'] = 100;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End resize*********/
                    }
                }
                if (empty($resize_error)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                } else {
//                            $this->output->set_status_header(402, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                }
            } else {
                if ($this->jjmedia->insert($post_data)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                }
            }
        }
    }

    function update($id){
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);
            $current_file = $this->file->where('id', $post_data['file_id'])->get();

            unset($post_data['uploaded']);
            unset($post_data['file']);

            if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                $file_data['file_name'] = $uploaded->file_name;
                $file_data['file_type'] = $uploaded->file_type;
                $file_data['size'] = $uploaded->file_size;
                $file_data['url'] = base_url() . 'uploads/';
                $file_data['path'] = getwdir() . 'uploads/';

                $file_id = $this->file->insert($file_data);
                if (!$file_id) {
                    log_massage('debug', 'Insert File error update jjmedia');
                    $this->output->set_status_header(500, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later.']));
                    exit;
                }
                if (file_exists($current_file->path . $current_file->file_name)) {
                    unlink($current_file->path . $current_file->file_name);
                    if (file_exists($current_file->path . 'thumb/' . $current_file->file_name)) {
                        unlink($current_file->path . 'thumb/' . $current_file->file_name);
                    }
                }
                $this->file->delete($current_file->id);

                $post_data['file_id'] = $file_id;

                if ($this->jjmedia->update($post_data,$id)) {
                    /*****Create Thumb Image****/
                    $img_cfg['source_image'] = getwdir() . 'uploads/' . $uploaded->file_name;
                    $img_cfg['maintain_ratio'] = TRUE;
                    $img_cfg['new_image'] = getwdir() . 'uploads/thumb/' . $uploaded->file_name;
                    $img_cfg['quality'] = 99;
                    $img_cfg['height'] = 50;
                    $img_cfg['master_dim'] = 'height';

                    $this->image_lib->initialize($img_cfg);
                    if (!$this->image_lib->resize()) {
                        $resize_error[] = $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();

                    /********End Thumb*********/

                    /*resize and create thumbnail image*/
                    if ($uploaded->file_size > 1024) {
                        $img_cfg['image_library'] = 'gd2';
                        $img_cfg['source_image'] = getwdir() . 'uploads/' . $uploaded->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getwdir() . 'uploads/' . $uploaded->file_name;
                        $img_cfg['height'] = 500;
                        $img_cfg['quality'] = 100;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End resize*********/
                    }
                }
                $resize_error = [];
                if (empty($resize_error)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                } else {
                    $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                }
            } elseif($this->jjmedia->update($post_data,$id)) {
                $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
            }else {
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['validation_error' => 'Please select images.']));
            }
        }
    }


    function delete_image($id)
    {
        $jjmedia = $this->jjmedia->with_file()->where('file_id',$id)->get();
        if ($jjmedia->file != null and $this->file->delete($jjmedia->file->id)) {
            if (file_exists(getwdir() . 'uploads/' . $jjmedia->file->file_name)) {
                unlink(getwdir() . 'uploads/' . $jjmedia->file->file_name);
            }
            $this->jjmedia->update(['file_id' => null], $jjmedia->id);
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Image Delete']));
        }else{
            $this->output->set_status_header(400, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later']));
        }
    }

    function upload()
    {
        // $config['upload_path'] = getwdir() . 'uploads';
        $config['upload_path'] ='./uploads/';
        // $file_data['path'] = getwdir() . 'uploads/';
        $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
        $config['max_size'] = 4096;
        $config['file_name'] = 'BRD_' . rand();
        $config['multi'] = 'ignore';
        $this->upload->initialize($config);
        if ($this->upload->do_upload('file')) {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
        }else{
            $this->output->set_status_header(401, 'File Upload Error');
            $this->output->set_content_type('application/json')->set_output($this->upload->display_errors('',''));
        }
    }



    public function delete($id)
    {
        $jjmedia = $this->jjmedia->with_file()->where('id', $id)->get();
        if ($jjmedia) {
            if ($jjmedia->file != null) {
                if ($this->file->delete($jjmedia->file->id)) {
                    if (file_exists($jjmedia->file->path . $jjmedia->file->file_name)) {
                        unlink($jjmedia->file->path . $jjmedia->file->file_name);
                        if (file_exists($jjmedia->file->path . 'thumb/' . $jjmedia->file->file_name)) {
                            unlink($jjmedia->file->path . 'thumb/' . $jjmedia->file->file_name);
                        }
                        if ($this->jjmedia->delete($id)) {
                            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Media Deleted']));
                        } else {
                            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Media not deleted but some files are deleted']));
                        }
                    } else {
                        $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Media file not exist in directory']));
                    }
                }
            } else {
                $this->jjmedia->delete($id);
                $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Media Deleted']));
            }
        } else {
            $this->output->set_status_header(500, 'Validation error');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


}