<?php
class Index extends CI_Controller {
    

        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url_helper');
            $this->load->helper('url');
            $this->load->library('session');
            $this->load->helper('form');
            $this->load->model('Media_model', 'media');
            $this->load->model('Team_model', 'team');
            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Events_model', 'events');
            $this->load->model('News_model', 'news');
            $this->load->model('Document_model', 'document');
        }

        public function index()
        {
            $data['medias'] = $this->media->with_file()->order_by('id', 'desc')->limit(9)->get_all();
            $data['testimonials'] = $this->testimonial->with_file()->get_all();
            $data['events'] = $this->events->with_file()->get_all();
            $data['news'] = $this->news->get_all();
            $data['document'] = $this->document->order_by('id', 'desc')->get();

            $this->load->view('index',$data);
        }

         public function overview()
        {
            $this->load->view('overview');
        }

         public function team()
        {
            $data['teams'] = $this->team->with_file()->where('designation', '!=', 'Managing Director')->order_by('designation')->get_all();
            $data['md'] = $this->team->with_file()->where('designation', 'Managing Director')->get();
            $this->load->view('team',$data);
        }

         public function vision()
        {
            $this->load->view('vision');
        }

         public function services()
        {
            $this->load->view('services');
        }

         public function news()
        {
            $data['newss'] = $this->news->with_files()->get_all();
            $this->load->view('news',$data);
        }

         public function media()
        {
            $data['medias'] = $this->media->with_file()->get_all();
            $this->load->view('media',$data);
        }

         public function contact()
        {
            $this->load->view('contact');
        }

        //  public function register()
        // {
        //     $this->load->view('register');
        // }

         public function faq()
        {
            $this->load->view('faq');
        }

         public function help()
        {
            $this->load->view('help');
        }

         public function support()
        {
            $this->load->view('support');
        }

        // services section
        public function electrical()
        {
            $this->load->view('electrical');
        }
        public function slrm()
        {
            $this->load->view('slrm');
        }
        public function AgricultureandAlliedActivities()
        {
            $this->load->view('AgricultureandAlliedActivities');
        }
        public function TechnicalServicesandMaintenance()
        {
            $this->load->view('TechnicalServicesandMaintenance');
        }
        public function TourismandTruckinginNilambur()
        {
            $this->load->view('TourismandTruckinginNilambur');
        }
        public function TransportationCoordinationforInstitutions()
        {
            $this->load->view('TransportationCoordinationforInstitutions');
        }
        public function DiaryDevelopmentProjects()
        {
            $this->load->view('DiaryDevelopmentProjects');
        }
        public function ConstructionandContractingWorks()
        {
            $this->load->view('ConstructionandContractingWorks');
        }
        public function HealthCareManagement()
        {
            $this->load->view('HealthCareManagement');
        }

        function send_mail()
        {
            $this->load->library('email');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'valid_email');

            if ($this->form_validation->run() === FALSE)
            {
                $this->output->set_status_header(400, 'Validation error');
            }else {

                $config = [
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://mail.pravasco.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'web@pravasco.com',
                    'smtp_pass' => 'web@pravasco',
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1',
                    'wordwrap' => TRUE
                ];

                

                $this->email->initialize($config);
                 
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $subject = $this->input->post('subject');
                $phone = $this->input->post('phone');
                $message = $this->input->post('message');

                $message = wordwrap($message, 70, "\n");

                // $subject = 'Contact Request From :  ' . $email;

                $content  = 'NAME    :   ' . $name . "\r\n";
                $content .= 'EMAIL   :  ' . $email . "\r\n";
                $content .= 'SUBJECT  : ' . $subject . "\r\n";
                $content .= 'PHONE   : ' . $phone . "\r\n" ;
                $content .= 'MESSAGE  :   ' . $message ."\r\n";

                // $content = str_replace("\n.", "\n.", $content);

                
                $this->email->set_newline("\r\n");
                $this->email->from('web@pravasco.com', 'Pravasco');
                $this->email->to('pravasconbr@gmail.com');
//                $this->email->to('noushidpsybo@gmail.com');
                $this->email->subject('Contact from Website-' . base_url());

                $this->email->message($content);

                if ($this->email->send()) {
                    if(isset($_SERVER['HTTP_REFERER']))
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect(base_url('index'), 'refresh');
                    }

                }
                else {
                    if(isset($_SERVER['HTTP_REFERER']))
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect(base_url('index'), 'refresh');
                    }
                }
            }
        }

        function register()
        {
            $this->load->view('register');

            $this->load->library('email');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'valid_email');

            if ($this->form_validation->run() === FALSE)
            {
                $this->output->set_status_header(400, 'Validation error');
            }else {

                $config = [            
                    'protocol' => 'smtp',           
                    'smtp_host' => 'ssl://mail.pravasco.com',
                    'smtp_port' => 465,            
                    'smtp_user' => 'web@pravasco.com',
                    'smtp_pass' => 'web@pravasco',
                    'mailtype' => 'html',            
                    'charset' => 'iso-8859-1',            
                    'wordwrap' => TRUE        
            ];
                $this->email->initialize($config);
                 
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $date = $this->input->post('appointment_date');
                $time = $this->input->post('appointment_time');
                $message = $this->input->post('message');



                $message = wordwrap($message, 70, "\n");

                // $subject = 'Contact Request From :  ' . $email;

                $content  = 'Name    :   ' . $name . "\r\n";
                $content .= 'Email   :  ' . $email . "\r\n";
                $content .= 'Phone   : ' . $phone . "\r\n" ;
                $content .= 'Appointment date  : ' . $date . "\r\n";
                $content .= 'Appointment time : ' . $time . "\r\n";
                $content .= 'Message  :   ' . $message ."\r\n";
                
                $this->email->set_newline("\r\n");

                $this->email->from('web@pravasco.com', 'Pravasco');
                $this->email->to('pravasconbr@gmail.com');
//                $this->email->to('noushidpsybo@gmail.com');
                $this->email->subject('Contact from Website-' . base_url());

                $this->email->message($content);

                if ($this->email->send()) {
                    // $this->output->set_output("Appoinment requested ");
                    echo "<script> alert('Success !!');
                        </script>";
                    redirect(base_url('register'),'refresh');
                }
                else {
                    echo "<script> alert('Success !!');
                        </script>";
                    redirect(base_url('register'),'refresh');
                }
            }
        }


    public function contactForm()
    {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.pravasco.com',
            'smtp_port' => 465,
            'smtp_user' => 'web@pravasco.com',
            'smtp_pass' => 'web@pravasco',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ];
        $this->email->initialize($config);

        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $date = $this->input->post('appointment_date');
        $time = $this->input->post('appointment_time');
        $message = $this->input->post('message');



        $message = wordwrap($message, 70, "\n");

        // $subject = 'Contact Request From :  ' . $email;

        $content  = 'Name    :   ' . $name . "\r\n";
        $content .= 'Email   :  ' . $email . "\r\n";
        $content .= 'Phone   : ' . $phone . "\r\n" ;
        $content .= 'Appointment date  : ' . $date . "\r\n";
        $content .= 'Appointment time : ' . $time . "\r\n";
        $content .= 'Message  :   ' . $message ."\r\n";

        $this->email->set_newline("\r\n");

        $this->email->from('web@pravasco.com', 'Pravasco');
                $this->email->to('pravasconbr@gmail.com');
//        $this->email->to('noushidpsybo@gmail.com');
        $this->email->subject('Contact from Website-' . base_url());

        $this->email->message($content);

        if ($this->email->send()) {
            // $this->output->set_output("Appoinment requested ");
            echo "<script> alert('Success !!');
                        </script>";
            redirect(base_url('register'),'refresh');
        }
        else {
            $this->output->set_status_header(400, 'Unable to send mail');
            $this->output->set_output("Please try again later.");
        }
    }
}