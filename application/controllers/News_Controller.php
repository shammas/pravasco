<?php


defined('BASEPATH') or exit('No Direct Script Access Allowed');

class News_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('File_model', 'file');
        $this->load->model('News_model', 'news');
        $this->load->model('News_file_model', 'news_file');
        $this->load->library(['upload', 'image_lib','ion_auth']);

        $this->load->library('form_validation');
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }


    }
    function index()
    {
        $data = $this->news->with_files()->get_all();
        // if ($data != false) {
        //     foreach ($data as $value) {
        //         $value->files = array_values((array) $value->files);
        //     }
        // }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

   
    function store()
    {
        $this->form_validation->set_rules('title', 'Title', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);
            $news_id = $this->news->insert($post_data);

            // if (!empty($uploaded) ) {
                /*INSERT FILE DATA TO DB*/
                foreach ($uploaded as $value) {
                    $file_data['file_name'] = $value->file_name;
                    $file_data['file_type'] = $value->file_type;
                    $file_data['size'] = $value->file_size;
                    $file_data['url'] = base_url() . 'uploads/';
                    $file_data['path'] = getcwd() . 'uploads/';

                    $file_id = $this->file->insert($file_data);

                    $news_file['news_id'] = $news_id;
                    $news_file['file_id'] = $file_id;

                    if ($this->news_file->insert($news_file)) {
                        /*****Create Thumb Image****/
                        $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getcwd() . 'uploads/thumb/thumb_' . $value->file_name;
                        $img_cfg['quality'] = 99;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End Thumb*********/

                        /*resize and create thumbnail image*/
                        if ($value->file_size > 1024) {
                            $img_cfg['image_library'] = 'gd2';
                            $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['height'] = 500;
                            $img_cfg['quality'] = 100;
                            $img_cfg['master_dim'] = 'height';

                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End resize*********/
                        }
                    }
                    $resize_error = [];
                    if (empty($resize_error)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                    } else {
//                            $this->output->set_status_header(402, 'Server Down');
                        $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                    }
                }
            // } else {
                // $this->output->set_status_header(400, 'Validation Error');
                // $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
            // }
        }
    }

    function update($id){
        $this->form_validation->set_rules('title', 'Title', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);
            unset($post_data['files']);

            // if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                foreach ($uploaded as $value) {
                    $file_data['file_name'] = $value->file_name;
                    $file_data['file_type'] = $value->file_type;
                    $file_data['size'] = $value->file_size;
                    $file_data['url'] = base_url() . 'uploads/';
                    $file_data['path'] = getcwd() . 'uploads/';

                    $file_id = $this->file->insert($file_data);

                    $news_file['file_id'] = $file_id;
                    $news_file['news_id'] = $id;

                    if ($this->news_file->insert($news_file)) {
                        /*****Create Thumb Image****/
                        $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getcwd() . 'uploads/thumb/thumb_' . $value->file_name;
                        $img_cfg['quality'] = 99;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End Thumb*********/

                        /*resize and create thumbnail image*/
                        if ($value->file_size > 1024) {
                            $img_cfg['image_library'] = 'gd2';
                            $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['height'] = 500;
                            $img_cfg['quality'] = 100;
                            $img_cfg['master_dim'] = 'height';

                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End resize*********/
                        }
                    }
                }
                $this->news->update($post_data, $id);
                $resize_error = [];
                if (empty($resize_error)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                } else {
                    $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                }
            // } else
                if ($this->news->update($post_data, $id))
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));

        }
    }

    function delete_image($id)
    {
        $news_file = $this->news_file->with_file()->where('file_id', $id)->get();

        if ($this->file->delete($news_file->file_id) and $this->news_file->delete($news_file->id)) {
            if (file_exists($news_file->file->path . $news_file->file->file_name)) {
                unlink($news_file->file->path . $news_file->file->file_name);
            }
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Image Deleted']));
        }else{
            $this->output->set_status_header(400, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later']));
        }
    }

    public function delete($id)
    {
        $news = $this->news->where('id',$id)->get();
        if ($news) {
            $news_files= $this->news_file->with_file()->where('news_id',$id)->get_all();
            if ($news_files) {
                foreach ($news_files as $file) {
                    if ($this->news_file->delete($file->id)) {
                        if ($this->file->delete($file->file_id)) {
                            if(file_exists(getcwd() . 'uploads/' . $file->file->file_name)){
                                unlink(getcwd() . 'uploads/' . $file->file->file_name);
                            }
                            $status = 1;
                        } else {
                            $status = 0;
                        }
                    }
                }
                if ($status == 1) {
                    if ($this->news->delete($id)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'News Deleted']));
                    }
                } elseif ($status == 0) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'News not deleted but some files are deleted']));
                }
            } else {
                if ($this->news->delete($id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'News Deleted']));
                } else {
                    $this->output->set_status_header(500, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Delete Error']));
                }
            }
        } else {
            $this->output->set_status_header(500, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


    function upload()
    {
        // $config['upload_path'] = getcwd() . 'uploads';
        $config['upload_path'] ='./uploads/';
        $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
        $config['max_size'] = 4096;
        $config['file_name'] = 'PRD_' . rand();
        $config['multi'] = 'ignore';
        $this->upload->initialize($config);
        if ($this->upload->do_upload('file')) {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
        }else{
            $this->output->set_status_header(401, 'File Upload Error');
            $this->output->set_content_type('application/json')->set_output($this->upload->display_errors('',''));
        }
    }
}