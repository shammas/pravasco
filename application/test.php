
<!-- Start main-content -->
<div class="main-content">
<!-- Section: home -->
<section id="home" class="divider">
<div class="container-fluid p-0">
<!-- Slider Revolution Start -->
<div class="rev_slider_wrapper">
<div class="rev_slider" data-version="5.0">
<ul>
<!-- SLIDE 1 -->
<li data-index="rs-1" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/bg/bg2.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
    <!-- MAIN IMAGE -->
    <img src="images/bg/bg2.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
    <!-- LAYERS -->

    <!-- LAYER NR. 1 -->
    <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-dark-transparent-light pl-30 pr-30"
         id="rs-1-layer-1"

         data-x="['center']"
         data-hoffset="['0']"
         data-y="['middle']"
         data-voffset="['-115']"
         data-fontsize="['30']"
         data-lineheight="['50']"
         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1000"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 7; white-space: nowrap; font-weight:600; border-radius:45px;">We Provide Total
    </div>

    <!-- LAYER NR. 2 -->
    <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent pl-40 pr-40"
         id="rs-1-layer-2"

         data-x="['center']"
         data-hoffset="['0']"
         data-y="['middle']"
         data-voffset="['-45']"
         data-fontsize="['48']"
         data-lineheight="['70']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1000"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 7; white-space: nowrap; font-weight:600; border-radius:45px;">Health Care Solution
    </div>

    <!-- LAYER NR. 3 -->
    <div class="tp-caption tp-resizeme text-center text-white"
         id="rs-1-layer-3"

         data-x="['center']"
         data-hoffset="['0']"
         data-y="['middle']"
         data-voffset="['30']"
         data-fontsize="['16']"
         data-lineheight="['28']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1400"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">Every day we bring hope to millions of children in the world's<br>  hardest places as a sign of God's unconditional love.
    </div>

    <!-- LAYER NR. 4 -->
    <div class="tp-caption tp-resizeme"
         id="rs-1-layer-4"

         data-x="['center']"
         data-hoffset="['0']"
         data-y="['middle']"
         data-voffset="['90']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;"
         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1400"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-theme-colored pl-20 pr-20" href="#">View Details</a>
    </div>
</li>

<!-- SLIDE 2 -->
<li data-index="rs-2" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/bg/bg1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
    <!-- MAIN IMAGE -->
    <img src="images/bg/bg1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
    <!-- LAYERS -->

    <!-- LAYER NR. 1 -->
    <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-dark-transparent-light pl-15 pr-15"
         id="rs-2-layer-1"

         data-x="['left']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['-110']"
         data-fontsize="['30']"
         data-lineheight="['50']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1000"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 7; white-space: nowrap; font-weight:600;">We Provide Total
    </div>

    <!-- LAYER NR. 2 -->
    <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent pl-15 pr-15"
         id="rs-2-layer-2"

         data-x="['left']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['-45']"
         data-fontsize="['48']"
         data-lineheight="['70']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1000"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 7; white-space: nowrap; font-weight:600;">Health Care Solution
    </div>

    <!-- LAYER NR. 3 -->
    <div class="tp-caption tp-resizeme text-white"
         id="rs-2-layer-3"

         data-x="['left']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['30']"
         data-fontsize="['16']"
         data-lineheight="['28']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1400"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">Every day we bring hope to millions of children in the world's<br>  hardest places as a sign of God's unconditional love.
    </div>

    <!-- LAYER NR. 4 -->
    <div class="tp-caption tp-resizeme"
         id="rs-2-layer-4"

         data-x="['left']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['90']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;"
         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1400"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-theme-colored pl-20 pr-20" href="#">View Details</a>
    </div>
</li>

<!-- SLIDE 3 -->
<li data-index="rs-3" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/bg/bg3.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
    <!-- MAIN IMAGE -->
    <img src="images/bg/bg3.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
    <!-- LAYERS -->

    <!-- LAYER NR. 1 -->
    <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-dark-transparent-light pl-15 pr-15"
         id="rs-3-layer-1"

         data-x="['right']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['-110']"
         data-fontsize="['30']"
         data-lineheight="['50']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1000"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 7; white-space: nowrap; font-weight:600;">We Provide Total
    </div>

    <!-- LAYER NR. 2 -->
    <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent pl-15 pr-15"
         id="rs-3-layer-2"

         data-x="['right']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['-45']"
         data-fontsize="['48']"
         data-lineheight="['70']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1000"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 7; white-space: nowrap; font-weight:600;">Health Care Solution
    </div>

    <!-- LAYER NR. 3 -->
    <div class="tp-caption tp-resizeme text-right text-white"
         id="rs-3-layer-3"

         data-x="['right']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['30']"
         data-fontsize="['16']"
         data-lineheight="['28']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;s:500"
         data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1400"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">Every day we bring hope to millions of children in the world's<br>  hardest places as a sign of God's unconditional love.
    </div>

    <!-- LAYER NR. 4 -->
    <div class="tp-caption tp-resizeme"
         id="rs-3-layer-4"

         data-x="['right']"
         data-hoffset="['30']"
         data-y="['middle']"
         data-voffset="['90']"

         data-width="none"
         data-height="none"
         data-whitespace="nowrap"
         data-transform_idle="o:1;"
         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
         data-start="1400"
         data-splitin="none"
         data-splitout="none"
         data-responsive_offset="on"
         style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-theme-colored pl-20 pr-20" href="#">View Details</a>
    </div>
</li>
</ul>
</div><!-- end .rev_slider -->
</div>
</div>
</section>

<!-- Section: Welcome -->
<section id="about">
    <div class="container pb-0 pt-110">
        <div class="row equal-height-inner">
            <div class="col-md-4 mb-sm-60">
                <div class="feature-icon-box bg-theme-colored">
                    <div class="p-30">
                        <div class="feature-icon bg-theme-colored">
                            <i class="fa fa-heartbeat text-white font-30"></i>
                        </div>
                        <h6 class="text-uppercase text-white">quick help</h6>
                        <h4 class="text-uppercase text-white">Emergency Case</h4>
                        <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis id explicabo quam quo nisi nihil ducimus, possimus, in aperiam, optio repellat commodi labore. Explicabo quam dolor sit.</p>
                        <a href="#" class="btn p-0 text-white mt-15 font-13">Contact Now <i class="fa fa-angle-double-right ml-5"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-sm-60">
                <div class="feature-icon-box bg-theme-colored">
                    <div class="p-30">
                        <div class="feature-icon bg-theme-colored">
                            <i class="fa fa-clock-o text-white font-30"></i>
                        </div>
                        <h6 class="text-uppercase text-white">Medinova 24/7 Emergency service</h6>
                        <h4 class="text-uppercase text-white mb-5">Opening Hours</h4>
                        <div class="opening-hourse">
                            <ul class="list-unstyled text-white">
                                <li class="clearfix"> <span> Monday - Friday </span>
                                    <div class="value"> 9.00 - 20.00 </div>
                                </li>
                                <li class="clearfix"> <span> Saturday </span>
                                    <div class="value"> 10.00 - 16.00 </div>
                                </li>
                                <li class="clearfix"> <span> Sunday </span>
                                    <div class="value"> 9.30 - 18.00 </div>
                                </li>
                            </ul>
                        </div>
                        <p class="mt-0 pt-10 text-white">Just make an appointment to get help from our experts</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-icon-box bg-theme-colored">
                    <div class="p-30">
                        <div class="feature-icon bg-theme-colored">
                            <i class="fa fa-user-md text-white font-30"></i>
                        </div>
                        <h6 class="text-uppercase text-white">Meet Our Doctors</h6>
                        <h4 class="text-uppercase text-white">Doctors time table</h4>
                        <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis id explicabo quam quo nisi nihil ducimus, possimus, in aperiam, optio repellat commodi labore. Explicabo quam dolor sit.</p>
                        <a href="#" class="btn p-0 text-white mt-15 font-13">Read More <i class="fa fa-angle-double-right ml-5"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section: Services -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h4 class="font-playfair text-uppercase mt-0">Our Services</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis voluptatibus neque, assumenda maxime. Eaque libero unde corrupti deleniti maxime ratione doloremque suscipit perferendis aperiam labore debitis atque odit neque possimus optio quo.</p>
                <div class="row mt-20 mb-sm-30">
                    <div class="col-sm-6">
                        <div class="service-icon-box"> <a href="#" class="pull-left mr-20"><i class="icon-ambulance14"></i></a>
                            <div class="mt-5">
                                <h6 class="mt-5">Emergency Care</h6>
                                <p class="font-11"><em>Lorem ipsum</em></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="service-icon-box"> <a href="#" class="pull-left mr-20"><i class="icon-illness"></i></a>
                            <div class="">
                                <h6 class="mt-5">Operation Theater</h6>
                                <p class="font-11"><em>Lorem ipsum</em></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="service-icon-box"> <a href="#" class="pull-left mr-20"><i class="icon-stethoscope10"></i></a>
                            <div class="">
                                <h6 class="mt-5">Outdoor Checkup</h6>
                                <p class="font-11"><em>Lorem ipsum</em></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="service-icon-box"> <a href="#" class="pull-left mr-20"><i class="icon-medical51"></i></a>
                            <div class="">
                                <h6 class="mt-5">Cancer Service</h6>
                                <p class="font-11"><em>Lorem ipsum</em></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="service-icon-box"> <a href="#" class="pull-left mr-20"><i class="icon-hospital35"></i></a>
                            <div class="">
                                <h6 class="mt-5">Blood Test</h6>
                                <p class="font-11"><em>Lorem ipsum</em></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="service-icon-box"> <a href="#" class="pull-left mr-20"><i class="icon-tablets9"></i></a>
                            <div class="">
                                <h6 class="mt-5">Pharmacy</h6>
                                <p class="font-11"><em>Lorem ipsum</em></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="thumb">
                    <img alt="" src="images/photos/4.jpg" class="img-fullwidth">
                </div>
                <h4 class="mt-20">Why Choose Us?</h4>
                <div class="panel-group accordion style2 mb-0 mt-20" id="accordion2">
                    <div class="panel">
                        <div class="panel-title"> <a href="#accordion21" data-toggle="collapse" data-parent="#accordion2"> <span class="open-sub"></span> Best Case Strategy </a> </div>
                        <div class="panel-collapse collapse" id="accordion21">
                            <div class="panel-content">
                                <p>Ut cursus massa at urnaaculis estie. Sed aliquamellus vitae ultrs condmentum leo massa mollis estiegittis miristum nulla.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-title"> <a href="#accordion22" data-toggle="collapse" data-parent="#accordion2"> <span class="open-sub"></span> Review your Case Documents </a> </div>
                        <div class="panel-collapse collapse" id="accordion22">
                            <div class="panel-content">
                                <p>Ut cursus massa at urnaaculis estie. Sed aliquamellus vitae ultrs condmentum leo massa mollis estiegittis miristum nulla.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-title"> <a href="#accordion23" data-toggle="collapse" data-parent="#accordion2"> <span class="open-sub"></span> Fight for Justice </a> </div>
                        <div class="panel-collapse collapse" id="accordion23">
                            <div class="panel-content">
                                <p>Ut cursus massa at urnaaculis estie. Sed aliquamellus vitae ultrs condmentum leo massa mollis estiegittis miristum nulla.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section: Specialities -->
<section>
    <div class="container pb-80">
        <div class="section-title text-center">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2 class="text-uppercase mt-0">Our Specialities</h2>
                    <div class="title-icon">
                        <img class="mb-10" src="images/title-icon.png" alt="">
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                </div>
            </div>
        </div>
        <div class="section-centent">
            <div class="row">
                <div class="col-md-12">
                    <div class="services-tab border-10px bg-white-light">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab11" data-toggle="tab"><i class="icon-xray2"></i>Orthopaedics</a></li>
                            <li><a href="#tab12" data-toggle="tab"><i class="icon-heart36"></i>Cardiology</a></li>
                            <li><a href="#tab13" data-toggle="tab"><i class="icon-brain9"></i>Neurology</a></li>
                            <li><a href="#tab14" data-toggle="tab"><i class="icon-teeth1"></i>Dental</a></li>
                            <li><a href="#tab15" data-toggle="tab"><i class="icon-hospital35"></i>Haematology</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab11">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="thumb">
                                            <img class="img-fullwidth" src="images/services/1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="service-content">
                                            <h3 class="sub-title mb-0 mt-30">Services</h3>
                                            <h1 class="title mt-0">Orthopaedics</h1>
                                            <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                                            <div class="row mt-30 mb-20">
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;General Medical</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Feel like Home Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Outdoor Checkup</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Easy and Affordable Billing</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <a class="btn btn-lg btn-dark btn-theme-colored" href="#">Book Appointment</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="thumb">
                                            <img class="img-fullwidth" src="images/services/2.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="service-content">
                                            <h3 class="sub-title mb-0 mt-30">Services</h3>
                                            <h1 class="title mt-0">Cardiology</h1>
                                            <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                                            <div class="row mt-30 mb-20">
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;General Medical</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Feel like Home Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Outdoor Checkup</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Easy and Affordable Billing</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <a class="btn btn-lg btn-dark btn-theme-colored" href="#">Book Appointment</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab13">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="thumb">
                                            <img class="img-fullwidth" src="images/services/3.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="service-content">
                                            <h3 class="sub-title mb-0 mt-30">Services</h3>
                                            <h1 class="title mt-0">Neurology</h1>
                                            <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                                            <div class="row mt-30 mb-20">
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;General Medical</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Feel like Home Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Outdoor Checkup</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Easy and Affordable Billing</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <a class="btn btn-lg btn-dark btn-theme-colored" href="#">Book Appointment</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab14">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="thumb">
                                            <img class="img-fullwidth" src="images/services/4.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="service-content">
                                            <h3 class="sub-title mb-0 mt-30">Services</h3>
                                            <h1 class="title mt-0">Dental</h1>
                                            <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                                            <div class="row mt-30 mb-20">
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;General Medical</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Feel like Home Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Outdoor Checkup</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Easy and Affordable Billing</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <a class="btn btn-lg btn-dark btn-theme-colored" href="#">Book Appointment</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab15">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="thumb">
                                            <img class="img-fullwidth" src="images/services/5.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="service-content">
                                            <h3 class="sub-title mb-0 mt-30">Services</h3>
                                            <h1 class="title mt-0">Haematology</h1>
                                            <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                                            <div class="row mt-30 mb-20">
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;General Medical</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-6">
                                                    <ul class="mt-10">
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Feel like Home Services</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Outdoor Checkup</li>
                                                        <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Easy and Affordable Billing</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <a class="btn btn-lg btn-dark btn-theme-colored" href="#">Book Appointment</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section: Features -->
<section>
    <div class="container-fluid pt-0 pb-0">
        <div class="row equal-height">
            <div class="col-md-4 bg-theme-colored sm-height-auto">
                <div class="p-60">
                    <h3 class="text-white mb-20 mt-0">Our Facilities</h3>
                    <ul class="list list-white check font-15">
                        <li><a href="#">Outpatient Rehabilitation</a></li>
                        <li><a href="#">Surgery & Transplants</a></li>
                        <li><a href="#">Gynaecological Clinic</a></li>
                        <li><a href="#">Primary Health Care</a></li>
                        <li><a href="#">Emergancy / Critical Care</a></li>
                        <li><a href="#">Outdoor Services</a></li>
                        <li><a href="#">Feel like Home Services</a></li>
                    </ul>
                    <a href="#" class="btn btn-default btn-lg mt-20">View Our Facilities</a>
                </div>
            </div>
            <div class="col-md-4 bg-img-cover sm-height-auto" data-bg-img="images/bg/bg4.jpg"></div>
            <div class="col-md-4 bg-theme-colored sm-height-auto">
                <div class="p-60">
                    <h3 class="text-white mb-20 mt-0">We Expert in</h3>
                    <ul class="list list-white check font-15">
                        <li><a href="#">Outpatient Rehabilitation</a></li>
                        <li><a href="#">Surgery & Transplants</a></li>
                        <li><a href="#">Gynaecological Clinic</a></li>
                        <li><a href="#">Primary Health Care</a></li>
                        <li><a href="#">Emergancy / Critical Care</a></li>
                        <li><a href="#">Outdoor Services</a></li>
                        <li><a href="#">Feel like Home Services</a></li>
                    </ul>
                    <a href="#" class="btn btn-default btn-lg mt-20">View Our Departments</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section: Doctors -->
<section>
    <div class="container pb-0">
        <div class="section-title text-center">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2 class="text-uppercase mt-0">Our Doctors</h2>
                    <div class="title-icon">
                        <img class="mb-10" src="images/title-icon.png" alt="">
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                </div>
            </div>
        </div>
        <div class="section-content">
            <div class="row multi-row-clearfix">
                <div class="col-md-12">
                    <div class="team-carousel-4col">
                        <div class="item">
                            <div class="team border-1px sm-text-center maxwidth400">
                                <div class="thumb"><img class="img-fullwidth" src="images/team/team1.jpg" alt=""></div>
                                <div class="content p-15 bg-white-light">
                                    <h4 class="name mb-0 mt-0"><a class="text-theme-colored" href="#">Dr. Abul Kalam Azadn</a></h4>
                                    <h6 class="title mt-0">MBBS,MD,PGDD</h6>
                                    <p class="mb-30">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                    <a class="btn btn-theme-colored btn-sm pull-right flip" href="#" data-toggle="modal" data-target="#myBooking">Book Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team border-1px sm-text-center maxwidth400">
                                <div class="thumb"><img class="img-fullwidth" src="images/team/team2.jpg" alt=""></div>
                                <div class="content p-15 bg-white-light">
                                    <h4 class="name mb-0 mt-0"><a class="text-theme-colored" href="#">Dr.P Abdul Kareem</a></h4>
                                    <h6 class="title mt-0">MBBS(Senior Consultent)</h6>
                                    <p class="mb-30">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                    <a class="btn btn-theme-colored btn-sm pull-right flip" href="#" data-toggle="modal" data-target="#myBooking">Book Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team border-1px sm-text-center maxwidth400">
                                <div class="thumb"><img class="img-fullwidth" src="images/team/team3.jpg" alt=""></div>
                                <div class="content p-15 bg-white-light">
                                    <h4 class="name mb-0 mt-0"><a class="text-theme-colored" href="#">Dr.Ramesh</a></h4>
                                    <h6 class="title mt-0">MBBS (Senior consultent)</h6>
                                    <p class="mb-30">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                    <a class="btn btn-theme-colored btn-sm pull-right flip" href="#" data-toggle="modal" data-target="#myBooking">Book Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team border-1px sm-text-center maxwidth400">
                                <div class="thumb"><img class="img-fullwidth" src="images/team/team4.jpg" alt=""></div>
                                <div class="content p-15 bg-white-light">
                                    <h4 class="name mb-0 mt-0"><a class="text-theme-colored" href="#">Dr.Mohammed Salahuddin</a></h4>
                                    <h6 class="title mt-0">MBBS ,DNB</h6>
                                    <p class="mb-30">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                    <a class="btn btn-theme-colored btn-sm pull-right flip" href="#" data-toggle="modal" data-target="#myBooking">Book Now</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section: Testimonials -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="testimonial style2 testimonial-carousel1">
                    <div class="item">
                        <div class="testimonial-wrapper text-center">
                            <div class="thumb"><img class="img-circle" alt="" src="images/testimonials/s1.jpg"></div>
                            <div class="content pt-10">
                                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque est quasi, quas ipsam, expedita placeat facilis odio illo ex accusantium eaque itaque officiis et sit. Vero quo, impedit neque.</p>
                                <i class="fa fa-quote-right font-36 mt-10 text-gray-lightgray"></i>
                                <h5 class="author text-theme-colored mb-0">Catherine Grace</h5>
                                <h6 class="title text-gray mt-0 mb-15">Designer</h6>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-wrapper text-center">
                            <div class="thumb"><img class="img-circle" alt="" src="images/testimonials/s2.jpg"></div>
                            <div class="content pt-10">
                                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque est quasi, quas ipsam, expedita placeat facilis odio illo ex accusantium eaque itaque officiis et sit. Vero quo, impedit neque.</p>
                                <i class="fa fa-quote-right font-36 mt-10 text-gray-lightgray"></i>
                                <h5 class="author text-theme-colored mb-0">Catherine Grace</h5>
                                <h6 class="title text-gray mt-0 mb-15">Designer</h6>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-wrapper text-center">
                            <div class="thumb"><img class="img-circle" alt="" src="images/testimonials/s3.jpg"></div>
                            <div class="content pt-10">
                                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque est quasi, quas ipsam, expedita placeat facilis odio illo ex accusantium eaque itaque officiis et sit. Vero quo, impedit neque.</p>
                                <i class="fa fa-quote-right font-36 mt-10 text-gray-lightgray"></i>
                                <h5 class="author text-theme-colored mb-0">Catherine Grace</h5>
                                <h6 class="title text-gray mt-0 mb-15">Designer</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section: Latest News and Updates -->
<section>
    <div class="container pb-100">
        <div class="section-title text-center">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2 class="text-uppercase mt-0">News</h2>
                    <div class="title-icon">
                        <img class="mb-10" src="images/title-icon.png" alt="">
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                </div>
            </div>
        </div>
        <div class="section-content">
            <div class="row multi-row-clearfix">
                <div class="blog-posts">
                    <div class="news-carousel-3col owl-nav-top">
                        <div class="item">
                            <article class="post style1 clearfix maxwidth500">
                                <div class="col-md-12 p-0">
                                    <div class="entry-header">
                                        <div class="post-thumb">
                                            <img src="images/blog/1.jpg" alt="" class="img-responsive img-fullwidth">
                                        </div>
                                        <div class="entry-date entry-date-absolute">
                                            25 <span>Dec</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="entry-content pl-50 p-20 pt-30 pr-20">
                                        <h5 class="entry-title pt-0"><a href="#">Different types of stroke</a></h5>
                                        <p>Lorem ipsum dolor adipisicing amet, consectetur sit elit. Aspernatur incidihil quo officia.</p>
                                        <a class="text-theme-colored mt-10 mb-0 pull-right flip" href="#">Read more <i class="fa fa-angle-double-right"></i></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="item">
                            <article class="post style1 clearfix maxwidth500">
                                <div class="col-md-12 p-0">
                                    <div class="entry-header">
                                        <div class="post-thumb">
                                            <img src="images/blog/1.jpg" alt="" class="img-responsive img-fullwidth">
                                        </div>
                                        <div class="entry-date entry-date-absolute">
                                            25 <span>Dec</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="entry-content pl-50 p-20 pt-30 pr-20">
                                        <h5 class="entry-title pt-0"><a href="#">Different types of stroke</a></h5>
                                        <p>Lorem ipsum dolor adipisicing amet, consectetur sit elit. Aspernatur incidihil quo officia.</p>
                                        <a class="text-theme-colored mt-10 mb-0 pull-right flip" href="#">Read more <i class="fa fa-angle-double-right"></i></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="item">
                            <article class="post style1 clearfix maxwidth500">
                                <div class="col-md-12 p-0">
                                    <div class="entry-header">
                                        <div class="post-thumb">
                                            <img src="images/blog/1.jpg" alt="" class="img-responsive img-fullwidth">
                                        </div>
                                        <div class="entry-date entry-date-absolute">
                                            25 <span>Dec</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="entry-content pl-50 p-20 pt-30 pr-20">
                                        <h5 class="entry-title pt-0"><a href="#">Different types of stroke</a></h5>
                                        <p>Lorem ipsum dolor adipisicing amet, consectetur sit elit. Aspernatur incidihil quo officia.</p>
                                        <a class="text-theme-colored mt-10 mb-0 pull-right flip" href="#">Read more <i class="fa fa-angle-double-right"></i></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="item">
                            <article class="post style1 clearfix maxwidth500">
                                <div class="col-md-12 p-0">
                                    <div class="entry-header">
                                        <div class="post-thumb">
                                            <img src="images/blog/1.jpg" alt="" class="img-responsive img-fullwidth">
                                        </div>
                                        <div class="entry-date entry-date-absolute">
                                            25 <span>Dec</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="entry-content pl-50 p-20 pt-30 pr-20">
                                        <h5 class="entry-title pt-0"><a href="#">Different types of stroke</a></h5>
                                        <p>Lorem ipsum dolor adipisicing amet, consectetur sit elit. Aspernatur incidihil quo officia.</p>
                                        <a class="text-theme-colored mt-10 mb-0 pull-right flip" href="#">Read more <i class="fa fa-angle-double-right"></i></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


</div>
<!-- end main-content -->