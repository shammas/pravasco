<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Index';
$route['dashboard/slider'] = 'Dashboard/loadslider';
$route['dashboard/packages'] = 'Dashboard/packages';

/*_______ public route start _______*/
$route['index'] = 'Index/index';
$route['overview'] = 'Index/overview';
$route['team'] = 'Index/team';
$route['vision'] = 'Index/vision';
$route['services'] = 'Index/services';
$route['news'] = 'Index/news';
$route['media'] = 'Index/media';
$route['contact'] = 'Index/contact';
$route['register'] = 'Index/register';
$route['faq'] = 'Index/faq';
$route['help'] = 'Index/help';
$route['support'] = 'Index/support';

/*_______ services route start _______*/
$route['ElectricalandElectronicsComponentsManufacturing'] = 'Index/electrical';
$route['SLRM'] = 'Index/slrm';
$route['AgricultureandAlliedActivities'] = 'Index/AgricultureandAlliedActivities';
$route['TechnicalServicesandMaintenance'] = 'Index/TechnicalServicesandMaintenance';
$route['TourismandTruckinginNilambur'] = 'Index/TourismandTruckinginNilambur';
$route['TransportationCoordinationforInstitutions'] = 'Index/TransportationCoordinationforInstitutions';
$route['DiaryDevelopmentProjects'] = 'Index/DiaryDevelopmentProjects';
$route['ConstructionandContractingWorks'] = 'Index/ConstructionandContractingWorks';
$route['HealthCareManagement'] = 'Index/HealthCareManagement';

 	
$route['index/email'] = 'Index/send_mail';
$route['index/register'] = 'Index/register';

/****Dashboard***/
$route['login'] = 'Auth/login';
$route['logout'] = 'Auth/logout';

$route['dashboard/load-user'] = 'Dashboard/load_user';
$route['dashboard/edit-user/(:num)']['POST'] = 'Auth/edit_user/$1';

$route['dashboard'] = 'Dashboard';
$route['dashboard/(:any)'] = 'Dashboard/page/$1';

$route['dashboard/news/get'] = 'News_Controller';
$route['dashboard/news/upload'] = 'News_Controller/upload';
$route['dashboard/news/add']['post'] = 'News_Controller/store';
$route['dashboard/news/edit/(:num)']['post'] = 'News_Controller/update/$1';
$route['dashboard/news/delete/(:num)']['delete'] = 'News_Controller/delete/$1';
$route['dashboard/news/delete-image/(:num)']['delete'] = 'News_Controller/delete_image/$1';

$route['dashboard/media/get'] = 'Media_Controller';
$route['dashboard/media/upload'] = 'Media_Controller/upload';
$route['dashboard/media/add']['post'] = 'Media_Controller/store';
$route['dashboard/media/edit/(:num)']['post'] = 'Media_Controller/update/$1';
$route['dashboard/media/delete/(:num)']['delete'] = 'Media_Controller/delete/$1';

$route['dashboard/testimonial/get'] = 'Testimonial_Controller';
$route['dashboard/testimonial/upload'] = 'Testimonial_Controller/upload';
$route['dashboard/testimonial/add']['post'] = 'Testimonial_Controller/store';
$route['dashboard/testimonial/edit/(:num)']['post'] = 'Testimonial_Controller/update/$1';
$route['dashboard/testimonial/delete/(:num)']['delete'] = 'Testimonial_Controller/delete/$1';

$route['dashboard/team/get'] = 'Team_Controller';
$route['dashboard/team/upload'] = 'Team_Controller/upload';
$route['dashboard/team/add']['post'] = 'Team_Controller/store';
$route['dashboard/team/edit/(:num)']['post'] = 'Team_Controller/update/$1';
$route['dashboard/team/delete/(:num)']['delete'] = 'Team_Controller/delete/$1';

$route['dashboard/events/get'] = 'Events_Controller';
$route['dashboard/events/upload'] = 'Events_Controller/upload';
$route['dashboard/events/add']['post'] = 'Events_Controller/store';
$route['dashboard/events/edit/(:num)']['post'] = 'Events_Controller/update/$1';
$route['dashboard/events/delete/(:num)']['delete'] = 'Events_Controller/delete/$1';

$route['dashboard/document/get'] = 'Document';
$route['dashboard/document/upload'] = 'Document/upload';
$route['dashboard/document/add']['post'] = 'Document/store';
$route['dashboard/document/edit/(:num)']['post'] = 'Document/update/$1';
$route['dashboard/document/delete/(:num)']['delete'] = 'Document/delete/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
