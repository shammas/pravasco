<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Nilambur Tourism and Support Services</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">
	<div class="body-overlay"></div>
	<div id="wrapper" class="clearfix">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>
		<!-- Header -->
		<header class="header">
			<div class="header-top bg-theme-colored sm-text-center">
		    	<div class="container">
	        		<div class="row">
		          		<div class="col-md-9">
		            		<div class="widget no-border m-0">
		              			<ul class="list-inline sm-pull-none sm-text-center mt-5">
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="#">+91 4931 297800</a> </li>
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="#">info@pravasco.com</a> </li>
		              			</ul>
		            		</div>
		          		</div>
	          			<div class="col-md-3">
		            		<div class="widget no-border m-0">
		              			<ul class="styled-icons pull-right icon-sm sm-text-center">
					                <li><a href="#" class="text-white"><i class="fa fa-facebook"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-twitter"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-google-plus"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-linkedin"></i></a></li>
		              			</ul>
		            		</div>
		          		</div>
		        	</div>
		      	</div>
		    </div>
			<div class="header-nav">
				<div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
					<div class="container">
						<nav id="menuzord-right" class="menuzord orange bg-lightest">
							<a class="menuzord-brand" href="javascript:void(0)"><img src="images/logo.png" alt=""></a>
							<ul class="menuzord-menu">
								<li><a href="index">Home</a></li>
								<li><a href="#home">About Us</a>
									<ul class="dropdown">
										<li><a href="overview">Overview</a></li>
										<li><a href="team">Our Team</a></li>
										<li><a href="vision">Our Vision</a></li>
									</ul>
								</li>
								<li class="active"><a href="services">Services</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
								<li><a href="contact">Contact Us</a></li>
								<li class="active"><a href="register" target="_blank">Register Now</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<!-- Start main-content -->
		<div class="main-content">

			<!-- Section: inner-header -->
			<section class="inner-header bg-black-222">
				<div class="container pt-10 pb-10">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-sm-8 xs-text-center">
								<h3 class="text-white mt-10">Nilambur Tourism and Support Services</h3>
							</div>
							<div class="col-sm-4">
								<ol class="breadcrumb white mt-10 text-right xs-text-center">
									<li><a href="#">Home</a></li>
									<li class="active">Our Activities</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section: About -->
			<section>
				<div class="container pb-0">
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored text-uppercase mt-0">Nilambur Tourism and Support Services</h3>
							<p>
								Kerala is considered as one of the best tourism destination across the world. Kerala selected as one of the ten paradises of the world by National Geographic Traveler, Kerala is famous especially for its ecotourism initiatives and beautiful backwaters. Its unique culture and traditions, coupled with its varied demography, have made Kerala one of the most popular tourist destinations in the world. The flow of tourists to Kerala and the tendency to explore new destinations will be given very good scope for the intervention
							</p>
							<h3 class="text-theme-colored">Nilambur as a tourism destination</h3>
							<p>
								Nilambur is located in Malappuram District of Kerala. Nestled at the bank of Chaliyar River in proximity to Nilgiri Range, this small town is popularly known as 'Land of Teak Plantations'. Nilambur is blessed with vibrant shades of natural beauty, the region is clustered with exotic wildlife and showcases a vivid illustrations of past through several royal residencies. Nilambur, covered under vast tracks of forest has many kind of wood mushrooming around other than teak. Venteak, rosewood and mahogany are the other few type of wood that flourish in the area. Pleasant weather is the last thread that makes this town a perfect destination for all kind of vacation makers.
							</p>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="post-thumb thumb">
                				<img src="images/services/Tourism and Trucking in Nilambur.jpg" alt="Pravsco Nilambur Tourism and Support Services" class="img-responsive img-fullwidth">
                			</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<h3 class="text-theme-colored text-uppercase mt-50">Nilambur Tourism Spots Identified</h3>
						</div>
						<div class="col-sm-12 col-md-6">
							<h4 class="text-theme-colored">Adyanpara Waterfalls</h4>
							<p>
								Offering a bewitching view, which is located in the Kumbalangod village, flanked by a thick cover of lush green forest, the water of this fall is believed to hold mystic medicinal properties and is naturally purified.
							</p>
							<h4 class="text-theme-colored">Elembalai Hill</h4>
							<p>
								Set amid a captivating surroundings of bamboo woods and sprawling dense forest, which is is a natural habitat of variety fauna including animals and birds like bisons, elephant, dogs, blue monkey, deer and wild cats.
							</p>
							<h4 class="text-theme-colored">Nedumkayam</h4>
							<p>
								A sprawling cover of rain forest situated about 18 km from Nilambur, is basket full of captivating views. The woods hold a rustic feel about them, guest house and forest station established under the British rule. The Forest Engineer Mr. Dozen developed the bridge and guest house here, who died in the same river while swimming, his burial ground is one of the attraction at Nedumkayam.
							</p>
							<h4 class="text-theme-colored">Teak Museum</h4>
							<p>
								Famous for being the world's first teak museum, this place is nature's most fascinating marvel. Located nearly 4 km from Nilambur Town, it is a place where one can get a whole bibliography about the teak wood including its history, culture, scientific information and aesthetic detail.
							</p>
							<h4 class="text-theme-colored">Conolly Plot</h4>
							<p>
								Sprawling across a huge area of 2.31 hectares is the famous teak plantation of Kerala also known as Conolly's Plot. Created between the years 1842 and 1844, it is renowned as the world's first teak plantation. World biggest and oldest teak tree is situated at Conolly Plot.
							</p>
							<h4 class="text-theme-colored">Central Forest Nursary</h4>
							<p>
								This is a place for plant lovers, where you will find plethora of plantation variety. Central Forest Nursery is one of the four central forest nurseries in Kerala established by Kerala Forest Department in year 1997. 
							</p>
							<h4 class="text-theme-colored">Central Teak Depot</h4>
							<p>
								Central Teak Depot is located near conolly plot and where the cut down teak trees stored for auction. 
							</p>
						</div>
						<div class="col-sm-12 col-md-6">
							<h4 class="text-theme-colored">Kakadampoyil</h4>
							<p>
								A similar place like Gavi, is a hamlet in the Kozhikode and Malappuram Districts and very close to Nilambur. This rural settlement is about 20 kilometers from Nilambur. There are many indigenous tribal groups in this area. It is set high on the Western Ghats, with altitudes ranging from 700 to 2100 m. It is considered as mini Ootty and beauty points, water falls, parks, wildlife, eco-friendly resorts etc are the attractions. Kozhipara water fall is most attractive one here. 
							</p>
							<h4 class="text-theme-colored">Keralakundu Waterfalls</h4>
							<p>
								This site is located in the middle of Silent Valley territory in a small village called Karuvarakkundu Near to Nilambur. It is also known as Kalkundu waterfalls, which gives a beautiful eye experience with opportunity for a natural bath in fresh water. 
							</p>
							<h4 class="text-theme-colored">Aruvacode</h4>
							<p>
								This village is located very close to Nilambur town and known as land of Potters and Pottery Works. The place is home to traditional potters who are slowly giving up the art owing to the increase in the use of substitutes.
							</p>
							<h4 class="text-theme-colored">Munderi</h4>
							<p>
								It is the first largest agricultural farm in Kerala and is owned by Govt. of Kerala (Dept. of Agriculture). There is variety of seeds and samplings being produced and distributed through government network. It is also a model agriculture farm, where farm tourists can have good exposure. 
							</p>
							<h4 class="text-theme-colored">Vazhikadavu and Nadukani</h4>
							<p>
								Vazhikkadavu is the border town of Kerala and where Nadugani Churam (Ghat Road) starts towards Tamilnadu. It is a beautiful experience to drive from Vazhikkadavu, the gate way of Malabar, to Nadukani and return, which gives beautiful natural sceneries and wild life experiences. 
							</p>
							<h4 class="text-theme-colored">Bunglaw Kunnu and Chaliyar Dormitory</h4>
							<p>
								It is located very close to Nilambur town and one of the best camping area with the capacity of 50 persons. Can enjoy the beauty of the forest and also nice location for small meeting as well as group activities. 
							</p>
							<h4 class="text-theme-colored">Nilambur Railway – Heritage line</h4>
							<p>
								Shoranur to Nilambur railway line is one of the oldest line and established in British period. 30 years back the line converted into broad gauge. The specialty of this line is, which is crossing through forest, farm land, rivers and streams and gives rich nature exposure. 
							</p>
						</div>
						<div class="col-md-12">
							<h3 class="text-theme-colored text-uppercase mt-50">Scope of the Activity </h3>
							<p>
								One of the major objective of the Kerala Government’s Tourism Policy 2017 is  to promote Responsible Tourism with active participation of the local bodies and citizens. Nilambur is one of the most suitable location to select for responsible tourism. The Policy also says that Kerala Tourism would provide the travellers with an opportunity to experience the rural life for the raw travelling experience. In such context by exploring rich diversity of Nilambur, PRAVASCO can develop a tourism based business opportunity to the entrepreneurs through various tourism package activities.
							</p>
							<h4>Activities Proposed</h4>
							<h5 class="text-theme-colored">1. Home Stays –</h5>
							<p class="ml-20">
								Kerala Tourism is promoting rural home stays, which can facilitate international as well as domestic travelers. Nilambur has wide scope for home stays since it is located in the main route to Ootty, Mysore, Bangalore and Wayanad. And also tourists can stay back one or two days as resting location with tourism experiences at above mentioned destinations.
							</p>
							<h5 class="text-theme-colored">2. Guided Tour – </h5>
							<p class="ml-20">
								All the above destinations can be experienced by the tourists through a guided tour, where a professional guide assist them to explore maximum destinations.
							</p>
							<h5 class="text-theme-colored">3. Package for Transportation – </h5>
							<p class="ml-20">
								Airport/Railway stations pick up to drop back or leave another destination will be offered transportation packages, Pravasco will lead and train the transportation co coordinators and entrepreneurs for the same.
							</p>
							<h5 class="text-theme-colored">4. Trucking and Wildlife Experiences – </h5>
							<p class="ml-20">
								Tourists can be taken to the inside forest areas for trucking and wild animals like elephants, deer, leopards, monkeys, various birds and butterflies etc may be seen while traveling. 
							</p>
							<h5 class="text-theme-colored">5. Exposure Trips to Tribal Settlement – </h5>
							<p class="ml-20">
								Nilambur has its own tradition, especially tribal from various communities are staying in the peripheries of the forest. Visiting the settlement, experiencing their culture, food and practices will be great opportunity to the academicians, travelers or interested groups.
							</p>
							<h5 class="text-theme-colored">6. Farming Tourism – </h5>
							<p class="ml-20">
								Nilambur has very good farming practices and many organic farmers performing with good results. The tourists can explore opportunities to live and involve in agriculture activities through the sponsored farming tourism.
							</p>
							<h5 class="text-theme-colored">7. Fishing and Cooking – </h5>
							<p class="ml-20">
								Many private fish farms, ponds and rivers can give nice experiences of fishing and self-cooking. 
							</p>
							<h5 class="text-theme-colored">8. Variety Food – </h5>
							<p class="ml-20">
								Malabar, Kerala, Nilambur food varieties can be enjoyed by the tourists, entreprenures, especially ladies can be selected by Pravasco for this activity.
							</p>
						</div>
						<div class="col-md-12">
							<h3 class="text-theme-colored mt-50">Online facility</h3>
							<p>
								A centralized web portal can manage the entire activities of tourism, company will manage the portal, promotion, booking, monitoring and review can be managed by this portal. A supported mobile app can provide to entrprenuers as well as custmers will help for on spot management.   
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<blockquote class="gray theme-colored">
			                	<p>
			                		Tourism is an emerging development sector in Kerala and considered as most viable, suitable and low investment required entrepreneurship sector. Since Nilmbur has the tourism destinations and also various travelers depend the route Pravasco can take major role to develop the tourism industry for Nilambur.
			                	</p>
			              	</blockquote>
			            </div>
					</div>
				</div>
			</section>

			<!-- divider: Emergency Services -->
			<section>
				<div class="container">
					<div class="section-content text-center">
						<div class="row">
							<div class="col-md-12">
								<h3 class="mt-0">We can help us</h3>
								<h2>Just call at <span class="text-theme-colored">+91 4931 297800</span> to make a reservation</h2>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- end main-content -->

		<!-- Footer -->
		<footer id="footer" class="footer pb-0" data-bg-img="<?php echo base_url('images/footer-bg.png')?>" data-bg-color="#25272e">
			<div class="container pb-20">
				<div class="row multi-row-clearfix">
					<div class="col-sm-6 col-md-5">
						<div class="widget dark"> <img alt="" src="<?php echo base_url('images/logo.png')?>">
							<p class="font-12 mt-20 mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">About Company</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="overview">Overview</a></li>
								<li><a href="team">Our Team</a></li>
								<li><a href="vision">Our Vision</a></li>
								<li><a href="services">Services</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Links</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="register">Register</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Contact</h5>
							<ul class="list-border">
								<li><a href="#">+91 4931 297800</a></li>
								<li><a href="#">info@pravasco.com</a></li>
								<li><a href="#" class="lineheight-20">Pravaso Pvt. Ltd, V K Road, Nilambur, Malappuram Dist, Kerala, 679 329</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-theme-colored p-15">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="text-white font-11 m-0">Copyright &copy;2019 PRAVASCO. All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<p class="text-white font-11 m-0 pull-right">Design & Developed By <a href="http://psybotechnologies.com" target="_blank"><img src="images/cloudbery.png"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.revolution.min.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>

</body>

</html>
