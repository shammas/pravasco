<!DOCTYPE html> 
<html dir="ltr" lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Home</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">
	<div class="body-overlay"></div>
	
	<div id="wrapper" class="clearfix">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>
		<!-- Header -->
		<header class="header">
			<div class="header-top bg-theme-colored sm-text-center">
		    	<div class="container">
	        		<div class="row">
		          		<div class="col-md-9">
		            		<div class="widget no-border m-0">
		              			<ul class="list-inline sm-pull-none sm-text-center mt-5">
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="#">+91 4931 297800</a> </li>
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="#">info@pravasco.com</a> </li>
		              			</ul>
		            		</div>
		          		</div>
	          			<div class="col-md-3">
		            		<div class="widget no-border m-0">
		              			<ul class="styled-icons pull-right icon-sm sm-text-center">
					                <li><a href="#" class="text-white"><i class="fa fa-facebook"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-twitter"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-google-plus"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-linkedin"></i></a></li>
		              			</ul>
		            		</div>
		          		</div>
		        	</div>
		      	</div>
		    </div>
			<div class="header-nav">
				<div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
					<div class="container">
						<nav id="menuzord-right" class="menuzord orange bg-lightest">
							<a class="menuzord-brand" href="javascript:void(0)"><img src="<?php echo base_url('images/logo.png')?>" alt=""></a>
							<ul class="menuzord-menu">
								<li><a href="index">Home</a></li>
								<li><a href="#home">About Us</a>
									<ul class="dropdown">
										<li><a href="overview">Overview</a></li>
										<li><a href="team">Our Team</a></li>
										<li><a href="vision">Our Vision</a></li>
									</ul>
								</li>
								<li class="active"><a href="services">Services</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
								<li><a href="contact">Contact Us</a></li>
								<li class="active"><a href="register" target="_blank">Register Now</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!-- Start main-content -->
		<div class="main-content">

			<!-- Section: inner-header -->
			<section class="inner-header bg-black-222">
				<div class="container pt-10 pb-10">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-sm-8 xs-text-center">
								<h3 class="text-white mt-10">Our Services</h3>
							</div>
							<div class="col-sm-4">
								<ol class="breadcrumb white mt-10 text-right xs-text-center">
									<li><a href="#">Home</a></li>
									<li class="active">Page Title</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section>
		     	<div class="container mt-30 mb-30 pt-30 pb-30">
		        	<div class="row">
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/TechnicalServicesandMaintenance.jpg" alt="Pravsco Technical Services and Maintenance" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="TechnicalServicesandMaintenance">Technical Services and Maintenance</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">Electrical and electronics manufacturing aims that design, manufacture, test, distribute, and...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="TechnicalServicesandMaintenance">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/slrm.jpg" alt="Pravsco Solid and Liquid Resource Management" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="SLRM">Solid and Liquid Resource Management</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="SLRM">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/Agriculture and Allied Activities.jpg" alt="Pravsco Agriculture and Allied Activities" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="AgricultureandAlliedActivities">Agriculture and Allied Activities</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="AgricultureandAlliedActivities">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/Electrical and Electronics Components Manufacturing.jpg" alt="Pravsco Electrical and Electronics Components Manufacturing" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="ElectricalandElectronicsComponentsManufacturing">Electrical and Electronics Components Manufacturing</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="ElectricalandElectronicsComponentsManufacturing">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          	</div>
		          	<div class="row">
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/Tourism and Trucking in Nilambur.jpg" alt="Pravsco Tourism and Trucking in Nilambur" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="TourismandTruckinginNilambur">Tourism and Trucking in Nilambur</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="TourismandTruckinginNilambur">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/Construction and Contracting Works.jpg" alt="Pravsco Construction and Contracting Works" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="ConstructionandContractingWorks">Construction and Contracting Works</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="ConstructionandContractingWorks">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/Diary Development Projects.jpg" alt="Pravsco Diary Development Projects" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="DiaryDevelopmentProjects">Dairy Development Projects</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="DiaryDevelopmentProjects">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/Health Care Management.jpg" alt="Pravsco Health Care Management" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="HealthCareManagement">Healthcare Management</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="HealthCareManagement">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          	</div>
		          	<div class="row">
		          		<div class="col-sm-6 col-md-3 col-lg-3">
		            		<article class="post clearfix maxwidth600 mb-30">
		              			<div class="entry-header">
		                			<div class="post-thumb thumb">
		                				<img src="images/services/Transportation Coordination for Institutions.jpg" alt="Pravsco Transportation Coordination for Institutions" class="img-responsive img-fullwidth">
		                			</div>
		              			</div>
				              	<div class="entry-content border-1px p-20">
				                	<h5 class="entry-title mt-0 pt-0"><a href="TransportationCoordinationforInstitutions">Transportation Coordination for Institutions</a></h5>
				                	<p class="text-left mb-20 mt-15 font-13">There has been a significant increase in waste generation in India in the last few decades, largely due to...</p>
				                	<a class="btn btn-dark btn-theme-colored btn-xs btn-flat pull-left mt-0" href="TransportationCoordinationforInstitutions">Read more</a>
				                	<div class="clearfix"></div>
				              	</div>
		            		</article>
		          		</div>
		          	</div>
		      	</div>
		    </section>

			<!-- Section: Services -->
			<!-- <section>
				<div class="container pb-0">
					<div class="row text-center">
					 <?php
					 if (isset($services) and $services != false) {
              			foreach ($services as $service) {
                      ?>
						<div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i
                                      class="fa <?php echo $service->icon;?> text-theme-colored"></i></a>
								<h5 class="icon-box-title"><?php echo $service->name; ?></h5>
								<p><?php echo $service->description; ?></p>
							</div>
						</div>
						<?php
						}
							}
						?> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-briefcase"></i></a>
								<h5 class="icon-box-title">Service Two</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-book"></i></a>
								<h5 class="icon-box-title">Service Three</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-gavel"></i></a>
								<h5 class="icon-box-title">Service One</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-briefcase"></i></a>
								<h5 class="icon-box-title">Service Two</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-book"></i></a>
								<h5 class="icon-box-title">Service Three</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-gavel"></i></a>
								<h5 class="icon-box-title">Service One</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-briefcase"></i></a>
								<h5 class="icon-box-title">Service Two</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
						<!-- <div class="col-sm-4">
							<div class="icon-box iconbox-theme-colored bg-light p-20">
								<a class="icon icon-dark icon-bordered icon-rounded icon-border-effect effect-rounded" href="#"><i class="fa fa-book"></i></a>
								<h5 class="icon-box-title">Service Three</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio,
									qui dicta alias.</p>
							</div>
						</div> -->
					<!-- </div>
				</div>
			</section> -->

		</div>

		<!-- Footer -->
		<footer id="footer" class="footer pb-0" data-bg-img="<?php echo base_url('images/footer-bg.png')?>" data-bg-color="#25272e">
			<div class="container pb-20">
				<div class="row multi-row-clearfix">
					<div class="col-sm-6 col-md-5">
						<div class="widget dark"> <img alt="" src="<?php echo base_url('images/logo.png')?>">
							<p class="font-12 mt-20 mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">About Company</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="overview">Overview</a></li>
								<li><a href="team">Our Team</a></li>
								<li><a href="vision">Our Vision</a></li>
								<li><a href="services">Services</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Links</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="register">Register</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Contact</h5>
							<ul class="list-border">
								<li><a href="#">+91 4931 297800</a></li>
								<li><a href="#">info@pravasco.com</a></li>
								<li><a href="#" class="lineheight-20">Pravaso Pvt. Ltd, V K Road, Nilambur, Malappuram Dist, Kerala, 679 329</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-theme-colored p-15">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="text-white font-11 m-0">Copyright &copy;2019 PRAVASCO. All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<p class="text-white font-11 m-0 pull-right">Design & Developed By <a href="http://psybotechnologies.com" target="_blank"><img src="images/cloudbery.png"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.revolution.min.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>

</body>

</html>
