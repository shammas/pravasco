<!DOCTYPE html> 
<html dir="ltr" lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Home</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="">
	<div id="wrapper">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>

		<!-- start main-content -->
		<div class="main-content">
			<!-- Section: home -->
			<section id="home" class="divider bg-deep">
				<div class="display-table">
					<div class="display-table-cell">
						<div class="container pb-100">
							<div class="row">
								<div class="col-md-6 col-md-push-3">
									<div class="text-center mb-60"><a href="#" class=""><img alt="" src="<?php echo base_url('images/logo.png')?>"></a></div>
									<div class="bg-light border-1px p-25">
										<h4 class="text-theme-colored text-uppercase m-0">Make an Appointment</h4>
										<div class="line-bottom mb-30"></div>
										<?php echo form_open_multipart('register', ['id' => 'appointment_form','name' => 'appointment_form']);
								?>
										<!-- <form id="appointment_form" name="appointment_form" class="mt-30" method="post" action="includes/appointment.php"> -->
											<div class="row">
												<div class="col-sm-12">
													<div class="form-group mb-10">
														<input name="name" class="form-control" type="text" required="" placeholder="Enter Name" aria-required="true">
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group mb-10">
														<input name="email" class="form-control required email" type="email" placeholder="Enter Email" aria-required="true">
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group mb-10">
														<input name="phone" class="form-control required" type="text" placeholder="Enter Phone" aria-required="true">
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group mb-10">
														<input name="appointment_date" class="form-control required date-picker" type="text" placeholder="Appoinment Date"
														  aria-required="true">
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group mb-10">
														<input name="appointment_time" class="form-control required time-picker" type="text" placeholder="Appoinment Time"
														  aria-required="true">
													</div>
												</div>
											</div>
											<div class="form-group mb-10">
												<textarea name="message" class="form-control required" placeholder="Enter Message" rows="5" aria-required="true"></textarea>
											</div>
											<div class="form-group mb-0 mt-20">
												<input name="form_botcheck" class="form-control" type="hidden" value="">
												<button type="submit" class="btn btn-dark btn-theme-colored" data-loading-text="Please wait...">Submit</button>
											</div>
										</form>
										<!-- Appointment Form Validation-->
										<script type="text/javascript">
											$("#appointment_form").validate({
												submitHandler: function(form) {
													var form_btn = $(form).find('button[type="submit"]');
													var form_result_div = '#form-result';
													$(form_result_div).remove();
													form_btn.before(
														'<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
													);
													var form_btn_old_msg = form_btn.html();
													form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
													$(form).ajaxSubmit({
														dataType: 'json',
														success: function(data) {
															if (data.status == 'true') {
																$(form).find('.form-control').val('');
															}
															form_btn.prop('disabled', false).html(form_btn_old_msg);
															$(form_result_div).html(data.message).fadeIn('slow');
															setTimeout(function() {
																$(form_result_div).fadeOut('slow')
															}, 6000);
														}
													});
												}
											});

										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- end main-content -->

		<!-- Footer -->
		<footer id="footer" class="footer text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="mb-0">Copyright ©2019 PRAVASCO. All Rights Reserved</p>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- Footer Scripts -->
	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>

</body>

</html>
