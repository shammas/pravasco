<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Home</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">
	<div class="body-overlay"></div>
	<div id="side-panel" class="dark" data-bg-img="http://placehold.it/1920x1280">
		<div class="side-panel-wrap">
			<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon_close font-30"></i></a></div>
			<a href="javascript:void(0)"><img alt="logo" src="images/logo.png"></a>
			<div class="side-panel-nav mt-30">
				<div class="widget no-border">
					<nav>
						<ul class="nav nav-list">
							<li><a href="#">Home</a></li>
							<li><a href="#">Services</a></li>
							<li><a class="tree-toggler nav-header">Pages <i class="fa fa-angle-down"></i></a>
								<ul class="nav nav-list tree">
									<li><a href="#">About</a></li>
									<li><a href="#">Terms</a></li>
									<li><a href="#">FAQ</a></li>
								</ul>
							</li>
							<li><a href="#">Contact</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="side-panel-widget mt-30">
				<div class="widget no-border">
					<ul>
						<li class="font-14 mb-5"> <i class="fa fa-phone text-theme-colored"></i> <a href="#" class="text-gray">123-456-789</a> </li>
						<li class="font-14 mb-5"> <i class="fa fa-clock-o text-theme-colored"></i> Mon-Fri 8:00 to 2:00 </li>
						<li class="font-14 mb-5"> <i class="fa fa-envelope-o text-theme-colored"></i> <a href="#" class="text-gray">contact@yourdomain.com</a>							</li>
					</ul>
				</div>
				<div class="widget">
					<ul class="styled-icons icon-dark icon-theme-colored icon-sm">
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
				<p>Copyright &copy;2016 ThemeMascot</p>
			</div>
		</div>
	</div>
	<div id="wrapper" class="clearfix">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>
		<!-- Header -->
		<header class="header">
			<div class="header-top bg-theme-colored sm-text-center">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div class="widget no-border m-0">
								<ul class="list-inline sm-text-center mt-8">
									<li>
										<a href="faq" class="text-white">FAQ</a>
									</li>
									<li class="text-white">|</li>
									<li>
										<a href="help" class="text-white">Help Desk</a>
									</li>
									<li class="text-white">|</li>
									<li>
										<a href="support" class="text-white">Support</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-md-4">
							<div class="widget no-border m-0">
								<ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-right sm-pull-none sm-text-center mt-sm-15">
									<li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus text-white"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header-nav">
				<div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
					<div class="container">
						<nav id="menuzord-right" class="menuzord orange bg-lightest">
							<a class="menuzord-brand" href="javascript:void(0)"><img src="images/logo.png" alt=""></a>
							<ul class="menuzord-menu">
								<li><a href="index">Home</a></li>
								<li><a href="#home">About Us</a>
									<ul class="dropdown">
										<li><a href="overview">Overview</a></li>
										<li><a href="team">Our Team</a></li>
										<li><a href="vision">Our Vision</a></li>
									</ul>
								</li>
								<li class="active"><a href="services">Services</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
								<li><a href="contact">Contact Us</a></li>
								<li class="active"><a href="register" target="_blank">Register Now</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<!-- Start main-content -->
		<div class="main-content">

			<!-- Section: inner-header -->
			<section class="inner-header bg-black-222">
				<div class="container pt-10 pb-10">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-sm-8 xs-text-center">
								<h3 class="text-white mt-10">Solid and Liquid Resource Management</h3>
							</div>
							<div class="col-sm-4">
								<ol class="breadcrumb white mt-10 text-right xs-text-center">
									<li><a href="#">Home</a></li>
									<li class="active">Our Activities</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section: About -->
			<section>
				<div class="container pb-0">
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored text-uppercase mt-0">Solid and Liquid Resource Management</h3>
							<p>
								There has been a significant increase in waste generation in India in the last few decades, largely due to rapid population growth and economic development. This is an attempt to explore business potential for private investors, especially Small & Medium Enterprises (SMEs), in Waste Management business. Waste management has come to be serious issue in Kerala as well. As per some estimates, on an average 6000 tons of Solid Waste is being generated in all across Kerala, in its 999 Panchayats, 53 Municipalities and 5 Corporations. On an average 6000 tons of Solid Waste is being generated in all across Kerala. Waste Management is an essential service to be provided by the municipal and local government authorities. Failure to provide it efficiently could be disastrous. Regular issues being faced many local bodies in connection with waste management is a contemporary topic.
							</p>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="post-thumb thumb">
                				<img src="images/services/slrm.jpg" alt="Pravsco Electrical and Electronics Component Manufacturing" class="img-responsive img-fullwidth">
                			</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored">Sources of Waste</h3>
							<p>
								<span class="text-highlight light">Solid Waste  –</span> there are two types of solid waste known degradable and non-degradable. <b>Degradable</b> wastes are organic in nature and can be processed and managed locally. <b>Non-degradable</b> wastes should be collected, stored and recycled for reuse it as in the converted form. 
							</p>
							<p>
								<span class="text-highlight light">Liquid Waste –</span> Non-hazardous liquid wastes can be treated, re used and finally re charged to the earth through ground water re charging units, where hazardous liquid waste (mainly chemical with content) to be treated and safely disposed with utmost care.
							</p>
							<p>
								<span class="text-highlight light">Digital Waste –</span> Digital waste is the contribution of new generation life and most complicated item, which is becoming an alarming issue in the present society. Use of alternate commodities, minimum usage and maximum re use are the remedial measures proposed in this field. All electronic gadgets, devices etc will be non-manageable waste in the future. 
							</p>
						</div>
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored mt-50">Reasons for growing waste</h3>
							<p>There are various reasons for growing municipal waste generation. Following are some of the reasons:</p>
							<ul class="list theme-colored">
								<li>Changing lifestyles</li>
								<li>Food habits</li>
								<li>Change in living standards</li>
								<li>Fast economic development</li>
								<li>Urbanization</li>
								<li>Growing tourism industry</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 ">
							<h3 class="text-theme-colored mt-50">Scope of the Area</h3>
							<b>Open Dumping and Land fill approach is not the lasting solution</b>
							<p>
								Open dumping has been found to create environmental problems because of air pollution, bad smell, presence of insects and rodents which are injurious to health, and potential contamination of ground water. This is the case with the most of the municipalities/panchayaths in Kerala. Lack of proper waste treatment facilities, the solution for them was dumping them in any available locations.
							</p>
							<p>
								Uncollected garbage- pileup and stinking waste across both sides of national highways of Kerala is a normal scene today. Piling up of garbage and litter and failure to adopt state of the art methods of waste management processes has serious consequences as follows:
							</p>
							<ul class="list angle-double-right theme-colored">
								<li>
									<b>Environmental</b>: pollution from poorly maintained landfill sites are prone to groundwater contamination and facilitate breeding of flies, mosquitoes, cockroaches, rats, and other pests.
								</li>
								<li>
									<b>Public health</b>: Possibility of frequent outbreaks of communicable diseases, such as Malaria, Dengue fever, Chickungunia etc, are enhanced
								</li>
								<li>
									<b>Economic effects</b>: can have negative impact on tourism industry 
								</li>
								<li>
									<b>Labor productivity</b> gets affected with frequent outbreaks of communicable diseases 
								</li>
							</ul>
						</div>
						<div class="col-sm-12 ">
							<h3 class="text-theme-colored mt-50">Intervention Proposed</h3>
							<p>
								In many countries waste is not a problem, instead they have considered it as resources for wealth. Private sector participation is one of the best choices open to boost the performance of public services like solid waste management. Local bodies are concentrated in only the collection of waste, instead if they can concentrate on segregating, processing and replacing the waste can be changed the entire scenario. Conversion of waste into re-usable status is money making business too but the co-operative or private sector need to support the governments in terms of management, technology integration, marketing and sales etc. Pravasco is envisaged to enter in this area and support the panchayth and municipalities to create a clean and green environment in their jurisdiction. 
							</p>
							<p>
								Waste Management is an art, which can be collected processed and re used, the waste as a resource to be collected from the following areas. 
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-bordered">
								<tr>
									<th>Source</th>
									<th>Typical waste generators</th>
									<th>Types of solid wastes</th>
								</tr>
								<tr>
									<td>Residential</td>
									<td>Single and multifamily dwellings</td>
									<td>Food wastes, paper, cardboard, plastics, textiles, leather, yard wastes, wood, glass, metals, ashes, special wastes (e.g. bulky items, consumer electronics, white goods, batteries, oil, tires), and household hazardous wastes </td>
								</tr>
								<tr>
									<td>Industrial</td>
									<td>Light and heavy manufacturing, fabrication, construction sites, power and chemical plants</td>
									<td>Housekeeping wastes, packaging, food wastes, construction and demolition materials, hazardous wastes, ashes, special wastes</td>
								</tr>
								<tr>
									<td>Commercial</td>
									<td>Stores, hotels, restaurants, markets, office buildings, etc.</td>
									<td>Paper, cardboard, plastics, wood, food wastes, glass, metals, special wastes, hazardous wastes</td>
								</tr>
								<tr>
									<td>Institutional</td>
									<td>Schools, hospitals, prisons, government centers</td>
									<td>Paper, cardboard, plastics, wood, food wastes, glass, metals, special wastes, hazardous wastes </td>
								</tr>
								<tr>
									<td>Construction and demolition</td>
									<td>New construction sites, road repair, renovation sites, demolition of buildings</td>
									<td>Wood, steel, concrete, dirt, etc</td>
								</tr>
								<tr>
									<td>Municipal services</td>
									<td>Street cleaning, landscaping, parks, beaches, other recreational areas, water and wastewater treatment plants</td>
									<td>Street sweepings, landscape and tree trimmings, general wastes from parks, beaches, and other recreational area, sludge</td>
								</tr>
								<tr>
									<td>Process</td>
									<td>Heavy and light manufacturing, refineries, chemical plants, power plants, mineral extraction and processing </td>
									<td>Industrial process wastes, scrap materials, offspecification products, slag, tailings </td>
								</tr>
								<tr>
									<td>Agriculture</td>
									<td>Crops, orchards, vineyards, dairies, feedlots, farms </td>
									<td>Spoiled food wastes, agricultural wastes, hazardous wastes (e.g. pesticides)</td>
								</tr>

							</table>

							<h4 class="text-theme-colored mt-50">Pravasco will give solutions for the following</h4>
							<ul class="list theme-colored">
								<li><b>Collection and transport of Wastes</b> – Kudumbashree members and other groups will be engaged for the collection and transport of the wastes from house holds, shops and institutions. The basic segregation will be done from the source by using double bin system to avoid the complications. </li>
								<li><b>Segregation of Wasteadmin
								</b> – A centralized unit will be established for large scales segregation and storage of waste, which would be considered as the raw material for the processing</li>
								<li><b>Processing Units </b>
									<ul class="list angle-double-right theme-colored">
										<li><b>Waste to Fertilizers</b> – It is a large area, mainly the solid degradable waste will be converted as fertilizer which will boost the agriculture and farming activities. </li>
										<li><b>Waste to Energy</b> – Electricity and bio gas can be generated from the degradable solid/liquid waste, technology integration is very much essential in this sector. Pravasco will tie up with technology partners and will provide grass root solutions for the same. </li>
										<li><b>Waste for Recycling</b> – Non degradable wastes normally collects and disposes through unsafe methods, it is highly hazardous to environment. Collection, storage and forwarding to the re cycle unit is the main activity here. Pravasco will engage the team to provide training to the house holds to keep the non-degradable waste, later collect, store and send to recycling units. Pravasco may set up a plastic shredding unit, and provide raw material for recycled products.    </li>
										<li><b>Waste for Disposal</b> – Hazardous waste should be processed and disposed safely, basically this is the responsibility of LSGs. Pravasco will have contracts with LSGs for safe disposal/industrial use of hazardous waste generated in their premises. </li>
									</ul>
								</li>
								<li><b>Liquid waste management</b>
									<ul class="list angle-double-right theme-colored">
										<li><b>House hold level Soak pits</b> – Pravasco will develop unique design and construct soak pits for households</li>
										<li><b>Sewage and Sludge Management</b> -  High end technical solution will be provided to the LSGs for Sewage and Sludge management, small scale treatment plants will be proposed for households also, where water will be processed and re used for farming purposes.</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<blockquote class="gray theme-colored">
			                	<p>
			                		Pravasco will train and keep resource for each sector, technology partners will also enlisted. This project is not only an income generation activity for the members but also a genuine need for the hour as the service to the humanity.  
			                	</p>
			              	</blockquote>
			            </div>
					</div>
				</div>
			</section>

			<!-- divider: Emergency Services -->
			<section>
				<div class="container">
					<div class="section-content text-center">
						<div class="row">
							<div class="col-md-12">
								<h3 class="mt-0">We can help us</h3>
								<h2>Just call at <span class="text-theme-colored">(01) 234 5678</span> to make a donation</h2>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- end main-content -->

		<!-- Footer -->
		<footer id="footer" class="footer pb-0" data-bg-img="<?php echo base_url('images/footer-bg.png')?>" data-bg-color="#25272e">
			<div class="container pb-20">
				<div class="row multi-row-clearfix">
					<div class="col-sm-6 col-md-5">
						<div class="widget dark"> <img alt="" src="<?php echo base_url('images/logo.png')?>">
							<p class="font-12 mt-20 mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">About Company</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="overview">Overview</a></li>
								<li><a href="team">Our Team</a></li>
								<li><a href="vision">Our Vision</a></li>
								<li><a href="services">Services</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Links</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="register">Register</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Contact</h5>
							<ul class="list-border">
								<li><a href="#">+91 4931 297800</a></li>
								<li><a href="#">info@pravasco.com</a></li>
								<li><a href="#" class="lineheight-20">Pravaso Pvt. Ltd, V K Road, Nilambur, Malappuram Dist, Kerala, 679 329</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-theme-colored p-15">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="text-white font-11 m-0">Copyright &copy;2019 PRAVASCO. All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<p class="text-white font-11 m-0 pull-right">Design & Developed By <a href="http://psybotechnologies.com" target="_blank"><img src="images/cloudbery.png"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.revolution.min.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>

</body>

</html>
