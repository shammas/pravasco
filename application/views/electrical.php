<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Home</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">
	<div class="body-overlay"></div>
	<div id="wrapper" class="clearfix">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>
		<!-- Header -->
		<header class="header">
			<div class="header-top bg-theme-colored sm-text-center">
		    	<div class="container">
	        		<div class="row">
		          		<div class="col-md-9">
		            		<div class="widget no-border m-0">
		              			<ul class="list-inline sm-pull-none sm-text-center mt-5">
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="#">+91 4931 297800</a> </li>
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="#">info@pravasco.com</a> </li>
		              			</ul>
		            		</div>
		          		</div>
	          			<div class="col-md-3">
		            		<div class="widget no-border m-0">
		              			<ul class="styled-icons pull-right icon-sm sm-text-center">
					                <li><a href="#" class="text-white"><i class="fa fa-facebook"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-twitter"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-google-plus"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-linkedin"></i></a></li>
		              			</ul>
		            		</div>
		          		</div>
		        	</div>
		      	</div>
		    </div>
			<div class="header-nav">
				<div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
					<div class="container">
						<nav id="menuzord-right" class="menuzord orange bg-lightest">
							<a class="menuzord-brand" href="javascript:void(0)"><img src="images/logo.png" alt=""></a>
							<ul class="menuzord-menu">
								<li><a href="index">Home</a></li>
								<li><a href="#home">About Us</a>
									<ul class="dropdown">
										<li><a href="overview">Overview</a></li>
										<li><a href="team">Our Team</a></li>
										<li><a href="vision">Our Vision</a></li>
									</ul>
								</li>
								<li class="active"><a href="services">Services</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
								<li><a href="contact">Contact Us</a></li>
								<li class="active"><a href="register" target="_blank">Register Now</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<!-- Start main-content -->
		<div class="main-content">

			<!-- Section: inner-header -->
			<section class="inner-header bg-black-222">
				<div class="container pt-10 pb-10">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-sm-8 xs-text-center">
								<h3 class="text-white mt-10">Electrical and Electronics Component Manufacturing</h3>
							</div>
							<div class="col-sm-4">
								<ol class="breadcrumb white mt-10 text-right xs-text-center">
									<li><a href="#">Home</a></li>
									<li class="active">Our Activities</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section: About -->
			<section>
				<div class="container pb-0">
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored text-uppercase mt-0">Electrical and Electronics Component Manufacturing</h3>
							<p>
								Electrical and electronics manufacturing aims that design, manufacture, test, distribute, and provide return/repair services for electrical and electronic components and assemblies for original equipment manufacturers (OEMs) or service providers (SPs).
							</p>
							<h5>Basic Electrical and Electronics Components</h5>
							<div class="col-xs-12">	
								<ul class="mt-10">
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Resistor - A resistor is an electrical component that restricts the flow of current in the circuit</li>
								</ul>
							</div>
							<div class="col-xs-6">
								<ul class="mt-10">
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Capacitor</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Microcontroller</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Inductor</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Integrated Circuits (ICs)</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Transformer</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Battery</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Fuse</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Diode</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; LED (Light Emitting Diode)</li>
								</ul>
							</div>
							<div class="col-xs-6">
								<ul class="mt-10">
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Crystals</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Terminals and Connectors</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Semi-Conductors</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Bulbs</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Memory Card/Pen Drive</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Solar Panels</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Invertors</li>
									<li><i class="fa fa-thumbs-o-up text-theme-colored"></i>&emsp; Telecommunication products</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="post-thumb thumb">
                				<img src="images/services/Electrical and Electronics Components Manufacturing.jpg" alt="Pravsco Electrical and Electronics Component Manufacturing" class="img-responsive img-fullwidth">
                			</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored mt-50">Scope of the Area</h3>
							<p>
								The above components are widely used in the industry and have high demand in the market. The production volume is not a problem to sale and it cannot to be match the demand and supply. Also the manufacturing of electrical and electronics components are not very difficult task, with proper training and practices the skill can achieved by any personnel, who are interested in this area. It is also having an additional advantage that, many NRKs work for electrical and electronics industries in abroad and used with high end/latest solutions in this field.  
							</p>
							<h3 class="text-theme-colored mt-50">Solar Projects</h3>
							<p>
								Solar energy is a unique and effective technology to solve the power/voltage issues faced by our local society. The Company will take up solar projects such as 
							</p>
							<ul class="list theme-colored">
								<li>Household solar power generation</li>
								<li>Solar Power for Institutions</li>
								<li>Solar Water Heater</li>
								<li>Solar Street Lights/Outdoor lights</li>
								<li>Solar Toilets etc</li>
							</ul>
						</div>
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored mt-50">Intervention Proposed</h3>
							<p>
								It is proposed to formulate various groups for manufacturing of different electrical and electronics components. Each group will function as independent business unit or a cottage industry. Company will co ordinate the following.
							</p>
							<ul class="list theme-colored">
								<li>Raw material sourcing and distribution</li>
								<li>Training and Technical Support</li>
								<li>Manufacturing/Assembling equipment supply</li>
								<li>Quality Check and Monitoring</li>
								<li>Collection, Packing and Storage of Products</li>
								<li>Branding and Marketing</li>
								<li>Sales and Profit Sharing</li>
							</ul>
						</div>
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored mt-50">Customized Projects</h3>
							<p>
								Company will also support the individual or organisations for any kind of customized electrical/electronic projects as demanded.
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<blockquote class="gray theme-colored">
			                	<p>
			                		Electrical and electronics products manufacturing business is perfect for those entrepreneurs who have technical skill and knowledge. The global electrical and electronics sector is highly fragmented, comprising of various auxiliary sectors namely electronic components, computer and office equipment, telecommunications, consumer appliances and industrial electronics. It is envisaged that the Pravasco can explore various areas of marketing and develop this unit as state of the art business unit based on the potential.
			                	</p>
			              	</blockquote>
			            </div>
					</div>
				</div>
			</section>

			<!-- divider: Emergency Services -->
			<section>
				<div class="container">
					<div class="section-content text-center">
						<div class="row">
							<div class="col-md-12">
								<h3 class="mt-0">We can help us</h3>
								<h2>Just call at <span class="text-theme-colored">(01) 234 5678</span> to make a donation</h2>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- end main-content -->

		<!-- Footer -->
		<footer id="footer" class="footer pb-0" data-bg-img="<?php echo base_url('images/footer-bg.png')?>" data-bg-color="#25272e">
			<div class="container pb-20">
				<div class="row multi-row-clearfix">
					<div class="col-sm-6 col-md-5">
						<div class="widget dark"> <img alt="" src="<?php echo base_url('images/logo.png')?>">
							<p class="font-12 mt-20 mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">About Company</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="overview">Overview</a></li>
								<li><a href="team">Our Team</a></li>
								<li><a href="vision">Our Vision</a></li>
								<li><a href="services">Services</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Links</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="register">Register</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Contact</h5>
							<ul class="list-border">
								<li><a href="#">+91 4931 297800</a></li>
								<li><a href="#">info@pravasco.com</a></li>
								<li><a href="#" class="lineheight-20">Pravaso Pvt. Ltd, V K Road, Nilambur, Malappuram Dist, Kerala, 679 329</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-theme-colored p-15">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="text-white font-11 m-0">Copyright &copy;2019 PRAVASCO. All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<p class="text-white font-11 m-0 pull-right">Design & Developed By <a href="http://psybotechnologies.com" target="_blank"><img src="images/cloudbery.png"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.revolution.min.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>

</body>

</html>
