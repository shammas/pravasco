<div class="wrapper wrapper-content animated fadeInRight"  >
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add document</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" ng-submit="addDocument()" >
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Name</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.name}">
                                <input type="text" placeholder="Your Name here" class="form-control" ng-model="newdocument.name">
                            </div>

                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationError.file,'has-error' : errFiles}" >
                            <label for="" class="control-label col-lg-1">Photo</label>
                            <div class="col-md-11 ">
                                <button ngf-select="uploadFiles($files, $invalidFiles)"
                                        ngf-max-size="5MB"
                                        ngf-multiple="true" type="button"
                                        class="upload-drop-zone btn-default"
                                        ngf-drop="uploadFiles($files)"
                                        ngf-drag-over-class="'drop'" ngf-multiple="false"
                                        ngf-pattern="'application/pdf'"
                                        ng-class="{'upload-drop-zone-error' : validationError.file,'upload-drop-zone-error' : errFiles}">
                                    <div class="dz-default dz-message">
                                        <span><strong>Drop files here or click to upload. </strong></span>
                                    </div>
                                </button>
                                <div class="help-block" ng-show="errFiles">Please Select valid file.</div>
                                <div class="help-block" ng-show="validationError.file">Please select image!</div>
                                <span class="alert alert-danger" ng-show="fileValidation.status == true">{{fileValidation.msg}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit"  ng-bind="(curdocument == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>
                    <div class="lightBoxGallery" ng-show="curdocument">
                        <a class="example-image-link" href="{{curdocument.file.url + curdocument.file.file_name}}" data-lightbox="item-list" data-title="">
                            <img src="{{curdocument.file.url + curdocument.file.file_name}}" width="100px">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3" ng-show="files">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload status</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <h5>{{files.name}}</h5>
                        <div class="lightBoxGallery">
                            <a class="example-image-link" href="{{files.$ngfBlobUrl}}" data-lightbox="example-1" data-title="">
                                <img ngf-src="files.$ngfBlobUrl" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                            </a>
                        </div>
                        <div class="progress">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar"
                                 class="progress-bar progress-bar-success"
                                 style="width:{{files.progress}}%" ng-show="uploadstatus != 1">
                                <span>{{files.progress}}% Complete</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" ng-repeat="f in errFiles">{{files.name}} {{files.$error}} {{files.$errorParam}}.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All documents</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="">
                        <button type="button" class="btn btn-primary" ng-click="newDocument()">
                            Update Investment application
                        </button>
                    </div>
                    <div class="table-responsive">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <table class="table table-striped table-bordered table-hover dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                                <thead>
                                <tr role="row">
                                    <th>Name</th>
                                    <th>File</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row">
                                    <td>{{documents.name}}</td>
                                    <td class="center">
                                        <a href="{{documents.url + documents.file_name}}" download>
                                            {{documents.file_name}}
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
