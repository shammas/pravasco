<!DOCTYPE html>
<html ng-app="nims">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PRAVASCO | Dashboard</title>
    <link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>font-awesome/css/font-awesome.css" rel="stylesheet">

     <!-- Toastr style -->
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

    <!-- Light box -->
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/lightbox.css" rel="stylesheet">

    <!-- Data table-->
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <!-- Data table-->
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/angular-confirm.css" rel="stylesheet">

    <!-- CLock Picker-->
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
    <link href="<?php echo base_url() . 'assets/dashboard/';?>css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/jquery-3.1.1.min.js"></script>
    <!-- Angular modules -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/angular-bootstrap.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/angularApp.js"></script>
   
</head>

<body ng-controller="AdminController">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span>
                               <img alt="image" class="img-circle" src="<?php echo base_url() . 'assets/dashboard/';?>img/profile_small.jpg" />
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Admin,PravasCo</strong>
                                 </span> <span class="text-muted text-xs block">Admin <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="#/profile">Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url('logout');?>">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    <li ng-class="{'active':url == 'dashboard'}">
                        <a href="#/"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                    </li>
                    <li ng-class="{'active':url == 'team'}">
                        <a href="#/team"><i class="fa fa-user-md"></i> <span class="nav-label">Our Team</span></a>
                    </li>
            
                    <li ng-class="{'active':url == 'media'}">
                        <a href="#/media"><i class="fa fa-picture-o"></i> <span class="nav-label">Media Centre</span></a>
                    </li>
                    <li ng-class="{'active':url == 'news'}">
                        <a href="#/news"><i class="fa fa-rss"></i> <span class="nav-label">News Release</span></a>
                    </li>
                     <li ng-class="{'active':url == 'events'}">
                        <a href="#/events"><i class="fa fa-sliders"></i> <span class="nav-label">Events</span></a>
                    </li>
                    <li ng-class="{'active':url == 'testimonial'}">
                        <a href="#/testimonial"><i class="fa fa-commenting-o"></i> <span class="nav-label">Testimonial</span></a>
                    </li>
                    <li ng-class="{'active':url == 'documents'}">
                        <a href="#/documents"><i class="fa fa-download"></i> <span class="nav-label">Documents</span></a>
                    </li>
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Welcome to Pravasco Admin.</span>
                        </li>

                        <li>
                            <a href="<?php echo base_url('logout');?>">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>

                </nav>
            </div>
            <div ng-view></div>

            <div class="footer">
                <div class="pull-right">
<!--                    10GB of <strong>250GB</strong> Free.-->
                </div>
                <div>
                    <strong>Copyright</strong> Psybo technologies &copy; 2017-2019
                </div>
            </div>
        </div>

    </div>
</div>



    <!-- Light box -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/lightbox-plus-jquery.min.js"></script>

    <!-- Mainly scripts -->

    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/inspinia.js"></script>
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/toastr/toastr.min.js"></script>

    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo base_url() . 'assets/dashboard/';?>js/plugins/clockpicker/clockpicker.js"></script>




<!--<script>-->
<!--    $(document).ready(function() {-->
<!--        setTimeout(function() {-->
<!--            toastr.options = {-->
<!--                closeButton: true,-->
<!--                progressBar: true,-->
<!--                showMethod: 'slideDown',-->
<!--                timeOut: 4000-->
<!--            };-->
<!--            toastr.success('Nims', 'Welcome to Dashboard');-->
<!---->
<!--        }, 1300);-->
<!---->
<!---->
<!--        $('.clockpicker').clockpicker();-->
<!---->
<!--    });-->
<!--</script>-->
</body>
</html>
