<!DOCTYPE html> 
<html dir="ltr" lang="en">
<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Home</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">
	<div class="body-overlay"></div>
	<div id="wrapper" class="clearfix">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>
		<!-- Header -->
		<header class="header">
			<div class="header-top bg-theme-colored sm-text-center">
		    	<div class="container">
	        		<div class="row">
		          		<div class="col-md-9">
		            		<div class="widget no-border m-0">
		              			<ul class="list-inline sm-pull-none sm-text-center mt-5">
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="#">+91 4931 297800</a> </li>
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="#">info@pravasco.com</a> </li>
		              			</ul>
		            		</div>
		          		</div>
	          			<div class="col-md-3">
		            		<div class="widget no-border m-0">
		              			<ul class="styled-icons pull-right icon-sm sm-text-center">
					                <li><a href="#" class="text-white"><i class="fa fa-facebook"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-twitter"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-google-plus"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-linkedin"></i></a></li>
		              			</ul>
		            		</div>
		          		</div>
		        	</div>
		      	</div>
		    </div>
			<div class="header-nav">
				<div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
					<div class="container">
						<nav id="menuzord-right" class="menuzord orange bg-lightest">
							<a class="menuzord-brand" href="javascript:void(0)"><img src="<?php echo base_url('images/logo.png')?>" alt=""></a>
							<ul class="menuzord-menu">
								<li class="active"><a href="index">Home</a></li>
								<li><a href="#home">About Us</a>
									<ul class="dropdown">
										<li><a href="overview">Overview</a></li>
										<li><a href="team">Our Team</a></li>
										<li><a href="vision">Our Vision</a></li>
									</ul>
								</li>
								<li><a href="services">Services</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
								<li><a href="contact">Contact Us</a></li>
								<li class="active"><a href="register" target="_blank">Register Now</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!-- Start main-content -->
		<div class="main-content">
			<!-- Section: home -->
			<section id="home" class="divider">
		      <div class="container-fluid p-0">
		        <!-- Slider Revolution Start -->
		        <div class="rev_slider_wrapper">
		          <div class="rev_slider" data-version="5.0">
		            <ul>
		              <!-- SLIDE 1 -->
		              <li data-index="rs-1" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/slide-1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
		                <!-- MAIN IMAGE -->
		                <img src="images/slide-1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
		                <!-- LAYERS -->
		                <!-- LAYER NR. 2 -->
		                <div class="tp-caption tp-resizeme text-gray"
		                  id="rs-1-layer-2"

		                  data-x="['left']"
		                  data-hoffset="['60']"
		                  data-y="['middle']"
		                  data-voffset="['100']" 
		                  data-fontsize="['58']"
		                  data-lineheight="['70']"

		                  data-width="none"
		                  data-height="none"
		                  data-whitespace="nowrap"
		                  data-transform_idle="o:1;s:500"
		                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
		                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
		                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
		                  data-start="1000" 
		                  data-splitin="none" 
		                  data-splitout="none" 
		                  data-responsive_offset="on"
		                  style="z-index: 7; white-space: nowrap; font-weight:800;">Together for a<br/>Better Social Living
		                </div>
		                <!-- LAYER NR. 4 -->
		                <div class="tp-caption tp-resizeme" 
		                  id="rs-1-layer-4"

		                  data-x="['left']"
		                  data-hoffset="['60']"
		                  data-y="['middle']"
		                  data-voffset="['205']"

		                  data-width="none"
		                  data-height="none"
		                  data-whitespace="nowrap"
		                  data-transform_idle="o:1;"
		                  data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
		                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
		                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
		                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
		                  data-start="1400" 
		                  data-splitin="none" 
		                  data-splitout="none" 
		                  data-responsive_offset="on"
		                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-flat btn-sm btn-theme-colored text-white" href="#">Our Services</a>
		                </div>
		              </li>
		              <!-- SLIDE 1 -->
		              <li data-index="rs-2" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/slide-2.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
		                <!-- MAIN IMAGE -->
		                <img src="images/slide-2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
		                <!-- LAYERS -->
		                <!-- LAYER NR. 2 -->
		                <div class="tp-caption tp-resizeme text-gray"
		                  id="rs-1-layer-2"

		                  data-x="['left']"
		                  data-hoffset="['60']"
		                  data-y="['middle']"
		                  data-voffset="['100']" 
		                  data-fontsize="['58']"
		                  data-lineheight="['70']"

		                  data-width="none"
		                  data-height="none"
		                  data-whitespace="nowrap"
		                  data-transform_idle="o:1;s:500"
		                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
		                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
		                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
		                  data-start="1000" 
		                  data-splitin="none" 
		                  data-splitout="none" 
		                  data-responsive_offset="on"
		                  style="z-index: 7; white-space: nowrap; font-weight:800;">Creating Endless Platform<br/>for the Returned NRKs
		                </div>
		                <!-- LAYER NR. 4 -->
		                <div class="tp-caption tp-resizeme" 
		                  id="rs-1-layer-4"

		                  data-x="['left']"
		                  data-hoffset="['60']"
		                  data-y="['middle']"
		                  data-voffset="['205']"

		                  data-width="none"
		                  data-height="none"
		                  data-whitespace="nowrap"
		                  data-transform_idle="o:1;"
		                  data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
		                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
		                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
		                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
		                  data-start="1400" 
		                  data-splitin="none" 
		                  data-splitout="none" 
		                  data-responsive_offset="on"
		                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-flat btn-sm btn-theme-colored text-white" href="#">Our Services</a>
		                </div>
		              </li>
		              <!-- SLIDE 1 -->
		              <li data-index="rs-3" data-transition="random" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/slide-3.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-description="">
		                <!-- MAIN IMAGE -->
		                <img src="images/slide-3.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
		                <!-- LAYERS -->
		                <!-- LAYER NR. 2 -->
		                <div class="tp-caption tp-resizeme text-gray"
		                  id="rs-1-layer-2"

		                  data-x="['left']"
		                  data-hoffset="['60']"
		                  data-y="['middle']"
		                  data-voffset="['100']" 
		                  data-fontsize="['58']"
		                  data-lineheight="['70']"

		                  data-width="none"
		                  data-height="none"
		                  data-whitespace="nowrap"
		                  data-transform_idle="o:1;s:500"
		                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
		                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
		                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
		                  data-start="1000" 
		                  data-splitin="none" 
		                  data-splitout="none" 
		                  data-responsive_offset="on"
		                  style="z-index: 7; white-space: nowrap; font-weight:800;">Together for a<br/>Better Social Living
		                </div>
		                <!-- LAYER NR. 4 -->
		                <div class="tp-caption tp-resizeme" 
		                  id="rs-1-layer-4"

		                  data-x="['left']"
		                  data-hoffset="['60']"
		                  data-y="['middle']"
		                  data-voffset="['205']"

		                  data-width="none"
		                  data-height="none"
		                  data-whitespace="nowrap"
		                  data-transform_idle="o:1;"
		                  data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
		                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
		                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
		                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
		                  data-start="1400" 
		                  data-splitin="none" 
		                  data-splitout="none" 
		                  data-responsive_offset="on"
		                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-flat btn-sm btn-theme-colored text-white" href="#">Our Services</a>
		                </div>
		              </li>
		            </ul>
		          </div>
		        </div>
		        <!-- end .rev_slider_wrapper -->
		        
		        <!-- Slider Revolution Ends -->

		      </div>
		    </section>

			<!-- Section: about -->
			<section id="about" class="bg-deep">
				<div class="container">
					<div class="row">
						<div class="col-md-7">
							<h4 class="m-0">Welcome to</h4>
							<h2 class="title text-theme-colored mt-0">Pravasco</h2>
							<p class="mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
							<h3 class="mt-20 mb-5">Our Vision</h3>
							<p class="mb-10">
								Create an endless platform to the NRKs and their dependents for the sustainable rehabilitation and towards leading a better social living.
							</p>
							<!-- <h3 class="mt-20 mb-5">Our Services</h3>
							<ul class="list theme-colored angle-double-right mb-sm-30">
								<li>Charity for Children & Education</li>
								<li>Setting up a charitable trust</li>
								<li>Services for Charities and Groups</li>
								<li>Planning to donate to good causes in your will</li>
							</ul> -->
						</div>
						<div class="col-md-5">
							<div class="thumb mt-5">
								<img alt="" src="<?php echo base_url('images/home.png')?>" class="img-fullwidth">
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section: featured project -->
			<!-- <section class="bg-deep">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-8 wow fadeInUp animation-delay3">
							<h4 class="text-uppercase line-bottom mt-0">Present Scenario</h4>
							<div class="volunteer-wrapper border-bottom sm-maxwidth500 clearfix pb-15 mb-sm-30">
								<p class="mb-10">
									It is considered that, one of the biggest threats to Kerala economy in the coming years would be substantial reduction in the remittance to the state due to increasing number of NRKs returning from Middle East. Following reasons indicated for the expected huge flow of NRKs back to home town. 
								</p>
								<ul class="list theme-colored angle-double-right mb-sm-30">
									<li>Emerging regionalism and change in work culture</li>
									<li>Oil crisis and price hike</li>
									<li>Inter-state conflicts in Middle East</li>
									<li>Economic crisis</li>
									<li>Introduction of Tax system</li>
									<li>Restrictions to family</li>
									<li>Expensive way of life forced to follow</li>
									<li>Illness and lack of facility for prompt care</li>
									<li>Underpayment and less employment opportunity</li>
									<li>Change in policies and liberal approach</li>
								</ul>
								<p class="mb-14">
									<a href="#" class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10">Join Us</a>
									<span class="pull-right ml-10 mt-12 font-14">volunteers</span>
									<span class="animate-number pull-right font-20 text-theme-colored lineheight-20 mt-5" data-value="8657"
									  data-animation-duration="2500">0</span>
								</p>
							</div>
						</div>
						<div class="col-sm-12 col-md-4 wow fadeInRight animation-delay3">
							<h4 class="text-uppercase line-bottom mt-0">Events</h4>
							<div class="bxslider bx-nav-top">
							 <?php
					            if (isset($events) and $events != false) {
					                foreach ($events as $event) {
			                    ?>
								<div class="event media sm-maxwidth400 p-15 mt-0 mb-14">
									<div class="row">
										<div class="col-xs-3 p-0">
											<div class="thumb pl-15">
												<img alt="No image" src="<?php echo $event->file->url . $event->file->file_name;?>" class="media-object">
											</div>
										</div>
										<div class="col-xs-6 p-0 pl-15">
											<div class="event-content">
												<h5 class="media-heading text-uppercase"><a href="#"><?php echo $event->title;?></a></h5>
												<p class="fs13"><?php echo $event->description;?></p>
											</div>
										</div>
										<div class="col-xs-3 pr-0">
											<div class="event-date text-center">
												<ul>
													<li class="font-36 text-theme-colored font-weight-700"><?php echo date('d', strtotime($event->created_at));?></li>
													<li class="font-20 text-center text-uppercase"><?php echo date('M', strtotime($event->created_at));?></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<?php
									}
										}
								?>
								
								
							</div>
						</div>
					</div>
				</div>
			</section> -->
            <?php
            if (isset($events) and $events != false) {
                ?>
                <section>
                    <div class="container pt-70">
                        <div class="section-title text-center">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <h3 class="text-uppercase mt-0">Recent Events</h3>

                                    <div class="title-icon">
                                        <i class="flaticon-charity-hand-holding-a-heart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="news-carousel owl-nav-top mb-sm-80" data-dots="true">
                                    <?php
                                    foreach ($events as $event) {
                                        ?>
                                        <!-- Single Item -->
                                        <div class="item">
                                            <article class="post clearfix maxwidth600 mb-sm-30 wow fadeInRight"
                                                     data-wow-delay=".2s">
                                                <div class="entry-header">
                                                    <div class="post-thumb thumb"><img
                                                            src="<?php echo $event->file->url . $event->file->file_name; ?>"
                                                            alt=""
                                                            class="img-responsive img-fullwidth">
                                                    </div>
                                                </div>
                                                <div class="entry-content border-1px p-20">
                                                    <h5 class="entry-title mt-0 pt-0"><a
                                                            href="#"><?php echo $event->title; ?></a></h5>

                                                    <p class="text-left mb-20 mt-15 font-13 fline"> <?php echo $event->description; ?></p>
                                                    <ul class="list-inline entry-date pull-right font-12 mt-5">
                                                        <li><a class="text-theme-colored" href="#">Admin |</a></li>
                                                        <li><span
                                                                class="text-theme-colored"><?php echo date('M d, Y', strtotime($event->created_at)); ?></span>
                                                        </li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </article>
                                        </div>
                                        <!-- Single Item -->
                                    <?php
                                    }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php
            }
            ?>

			<!-- Section: Contact -->
			<section id="contact" class="bg-deep">
				<div class="container pt-70">
					<div class="section-title text-center">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<h3 class="text-uppercase mt-0">Contact Us</h3>
								<div class="title-icon">
									<i class="flaticon-charity-hand-holding-a-heart"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="section-content">
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<div class="icon-box left media bg-light p-30 mb-20">
											<a class="media-left pull-left" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
											<div class="media-body"> <strong>OUR OFFICE LOCATION</strong>
												<p>V K Road, Nilambur, Malappuram, Kerala, Pin: 679-329</p>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-12">
										<div class="icon-box left media bg-light p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-call text-theme-colored"></i></a>
											<div class="media-body"> <strong>OUR CONTACT NUMBER</strong>
												<p> +91 4931-297800</p>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-12">
										<div class="icon-box left media bg-light p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-mail text-theme-colored"></i></a>
											<div class="media-body"> <strong>OUR CONTACT E-MAIL</strong>
												<p>info@pravasco.com</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<h3 class="mt-0 mb-30">Interested in discussing?</h3>
								<!-- Contact Form -->
								<!-- <?php echo form_open_multipart('email', ['id' => 'contact_form','name' => 'contact_form']);
								?> -->
								<form id="contact_form" name="contact_form" class="" action="index/email" method="post">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="form_name">Name <small>*</small></label>
												<input id="name" name="name" class="form-control" type="text" placeholder="Enter Name" required="">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="form_email">Email <small>*</small></label>
												<input id="email" name="email" class="form-control required email" type="email" placeholder="Enter Email">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="form_name">Subject <small>*</small></label>
												<input id="subject" name="subject" class="form-control required" type="text" placeholder="Enter Subject">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="form_phone">Phone</label>
												<input id="phone" name="phone" class="form-control" type="text" placeholder="Enter Phone">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="form_name">Message</label>
										<textarea id="message" name="message" class="form-control required" rows="5" placeholder="Enter Message"></textarea>
									</div>
									<div class="form-group">
										<input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
										<button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5" data-loading-text="Please wait...">Send your message</button>
										<button type="reset" class="btn btn-default btn-flat btn-theme-colored">Reset</button>
									</div>
								</form>

								<!-- Contact Form Validation-->
								<script type="text/javascript">
									$("#contact_form").validate({
										submitHandler: function(form) {
											var form_btn = $(form).find('button[type="submit"]');
											var form_result_div = '#form-result';
											$(form_result_div).remove();
											form_btn.before(
												'<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
											);
											var form_btn_old_msg = form_btn.html();
											form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
											$(form).ajaxSubmit({
												dataType: 'json',
												success: function(data) {
													if (data.status == 'true') {
														$(form).find('.form-control').val('');
													}
													form_btn.prop('disabled', false).html(form_btn_old_msg);
													$(form_result_div).html(data.message).fadeIn('slow');
													setTimeout(function() {
														$(form_result_div).fadeOut('slow')
													}, 6000);
												}
											});
										}
									});

								</script>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section: Testimonials and Logo -->
			<section class="bg-deep">
				<div class="container pt-0 pb-0">
					<div class="row equal-height">
                        <?php
                        if (isset($testimonials) and $testimonials != false) {
                            ?>
                            <div class="col-md-5 bg-deep">
                                <div class="pt-20 pb-20 pl-20 pr-20">
                                    <h4 class="line-bottom text-uppercase mt-0">Our People Say</h4>

                                    <div class="testimonial-carousel owl-nav-top">
                                        <?php
                                        foreach ($testimonials as $testimonial) {
                                            ?>
                                            <div class="item">
                                                <div class="testimonial-wrapper text-center">
                                                    <div class="thumb"><img class="img-circle" alt=""
                                                                            src="<?php echo $testimonial->file->url . $testimonial->file->file_name; ?>">
                                                    </div>
                                                    <div class="content pt-10">
                                                        <p>
                                                            <?php echo $testimonial->description; ?>
                                                        </p>
                                                        <i class="fa fa-quote-right font-36 mt-10 text-gray-lightgray"></i>
                                                        <h5 class="author text-theme-colored mb-0"><?php echo $testimonial->name; ?></h5>
                                                        <h6 class="title text-gray mt-0 mb-15"><?php echo $testimonial->designation; ?></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>


                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
						<div class="col-md-7">
							<div class="gallery-isotope grid-3 gutter-small clearfix" data-lightbox="gallery">
								<!-- Portfolio Item End -->
							<?php
                               	if (isset($medias) && $medias != false) {
                                    foreach ($medias as $media) {
                                        ?>
                                        <!-- Portfolio Item Start -->
                                        <div class="gallery-item">
                                            <div class="thumb">
                                                <img alt="project" src="<?php echo $media->file->url . $media->file->file_name;?>"
                                                     style="width:350px;height:170px;" class="img-fullwidth">

                                                <div class="overlay-shade"></div>
                                                <div class="icons-holder">
                                                    <div class="icons-holder-inner">
                                                        <div
                                                            class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                            <a href="<?php echo $media->file->url . $media->file->file_name;?>"
                                                               data-lightbox-gallery="gallery"><i
                                                                    class="fa fa-picture-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Portfolio Item End -->
                                    <?php
                                    }
                                }
                            ?>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>

		<!-- Footer -->
		<footer id="footer" class="footer pb-0" data-bg-img="<?php echo base_url('images/footer-bg.png')?>" data-bg-color="#25272e">
			<div class="container pb-20">
				<div class="row multi-row-clearfix">
					<div class="col-sm-6 col-md-5">
						<div class="widget dark"> <img alt="" src="<?php echo base_url('images/logo.png')?>">
							<p class="font-12 mt-20 mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">About Company</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="overview">Overview</a></li>
								<li><a href="team">Our Team</a></li>
								<li><a href="vision">Our Vision</a></li>
								<li><a href="services">Services</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Links</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="register">Register</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Contact</h5>
							<ul class="list-border">
								<li><a href="#">+91 4931 297800</a></li>
								<li><a href="#">info@pravasco.com</a></li>
								<li><a href="#" class="lineheight-20">Pravaso Pvt. Ltd, V K Road, Nilambur, Malappuram Dist, Kerala, 679 329</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-theme-colored p-15">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="text-white font-11 m-0">Copyright &copy;2019 PRAVASCO. All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<p class="text-white font-11 m-0 pull-right">Design & Developed By <a href="http://psybotechnologies.com" target="_blank"><img src="images/cloudbery.png"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.revolution.min.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>
	<script>
      $(document).ready(function(e) {
        var revapi = $(".rev_slider").revolution({
          sliderType:"standard",
          sliderLayout: "auto",
          dottedOverlay: "none",
          delay: 5000,
          navigation: {
              keyboardNavigation: "off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation: "off",
              onHoverStop: "off",
              touch: {
                  touchenabled: "on",
                  swipe_threshold: 75,
                  swipe_min_touches: 1,
                  swipe_direction: "horizontal",
                  drag_block_vertical: false
              },
              arrows: {
                  style: "gyges",
                  enable: true,
                  hide_onmobile: false,
                  hide_onleave: true,
                  hide_delay: 200,
                  hide_delay_mobile: 1200,
                  tmp: '',
                  left: {
                      h_align: "left",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  },
                  right: {
                      h_align: "right",
                      v_align: "center",
                      h_offset: 0,
                      v_offset: 0
                  }
              },
                bullets: {
                enable: true,
                hide_onmobile: true,
                hide_under: 800,
                style: "hebe",
                hide_onleave: false,
                direction: "horizontal",
                h_align: "center",
                v_align: "bottom",
                h_offset: 0,
                v_offset: 30,
                space: 5,
                tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
            }
          },
          responsiveLevels: [1240, 1024, 778],
          visibilityLevels: [1240, 1024, 778],
          gridwidth: [1170, 1024, 778, 480],
          gridheight: [620, 768, 960, 720],
          lazyType: "none",
          parallax: {
              origo: "slidercenter",
              speed: 1000,
              levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
              type: "scroll"
          },
          shadow: 0,
          spinner: "off",
          stopLoop: "on",
          stopAfterLoops: 0,
          stopAtSlide: -1,
          shuffle: "off",
          autoHeight: "off",
          fullScreenAutoWidth: "off",
          fullScreenAlignForce: "off",
          fullScreenOffsetContainer: "",
          fullScreenOffset: "0",
          hideThumbsOnMobile: "off",
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
              simplifyAll: "off",
              nextSlideOnWindowFocus: "off",
              disableFocusListener: false,
          }
        });
      });
    </script>

</body>

</html>
