<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Home</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png')?>" rel="shortcut icon" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">
	<div class="body-overlay"></div>
	<div id="wrapper" class="clearfix">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>
		<!-- Header -->
		<header class="header">
			<div class="header-top bg-theme-colored sm-text-center">
		    	<div class="container">
	        		<div class="row">
		          		<div class="col-md-9">
		            		<div class="widget no-border m-0">
		              			<ul class="list-inline sm-pull-none sm-text-center mt-5">
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="#">+91 4931 297800</a> </li>
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="#">info@pravasco.com</a> </li>
		              			</ul>
		            		</div>
		          		</div>
	          			<div class="col-md-3">
		            		<div class="widget no-border m-0">
		              			<ul class="styled-icons pull-right icon-sm sm-text-center">
					                <li><a href="#" class="text-white"><i class="fa fa-facebook"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-twitter"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-google-plus"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-linkedin"></i></a></li>
		              			</ul>
		            		</div>
		          		</div>
		        	</div>
		      	</div>
		    </div>
			<div class="header-nav">
				<div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
					<div class="container">
						<nav id="menuzord-right" class="menuzord orange bg-lightest">
							<a class="menuzord-brand" href="javascript:void(0)"><img src="<?php echo base_url('images/logo.png')?>" alt=""></a>
							<ul class="menuzord-menu">
								<li><a href="index">Home</a></li>
								<li><a href="#home">About Us</a>
									<ul class="dropdown">
										<li><a href="overview">Overview</a></li>
										<li><a href="team">Our Team</a></li>
										<li><a href="vision">Our Vision</a></li>
									</ul>
								</li>
								<li><a href="services">Services</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
								<li class="active"><a href="contact">Contact Us</a></li>
								<li class="active"><a href="register" target="_blank">Register Now</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!-- Start main-content -->
		<div class="main-content">

			<!-- Section: inner-header -->
			<section class="inner-header bg-black-222">
				<div class="container pt-10 pb-10">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-sm-8 xs-text-center">
								<h3 class="text-white mt-10">Contact Us</h3>
							</div>
							<div class="col-sm-4">
								<ol class="breadcrumb white mt-10 text-right xs-text-center">
									<li><a href="#">Home</a></li>
									<li class="active">Contact</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section: Have Any Question -->
			<section class="divider">
				<div class="container pt-60 pb-60">
					<div class="section-title mb-60">
						<div class="row">
							<div class="col-md-12">
								<div class="esc-heading small-border text-center">
									<h3>Have any Questions?</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="section-content">
						<div class="row">
							<div class="col-sm-12 col-md-4">
								<div class="contact-info text-center">
									<i class="fa fa-phone font-36 mb-10 text-theme-colored"></i>
									<h4>Call Us</h4>
									<h6>Phone: +91 4931 297800</h6>
								</div>
							</div>
							<div class="col-sm-12 col-md-4">
								<div class="contact-info text-center">
									<i class="fa fa-map-marker font-36 mb-10 text-theme-colored"></i>
									<h4>Address</h4>
									<h6>V K Road, Nilambur, Malappuram, <br/>Kerala, 679 329</h6>
								</div>
							</div>
							<div class="col-sm-12 col-md-4">
								<div class="contact-info text-center">
									<i class="fa fa-envelope font-36 mb-10 text-theme-colored"></i>
									<h4>Email</h4>
									<h6>info@pravasco.com</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section Contact -->
			<section id="contact" class="divider bg-lighter">
				<div class="container-fluid pt-0 pb-0">
					<div class="section-content">
						<div class="row">
							<div class="col-sm-12 col-md-6 bg-img-center bg-img-cover p-0" >
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1956.407623869141!2d76.22761795415339!3d11.274987492110938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xda3880f10cdfdc11!2sPravasco+Pvt+Ltd!5e0!3m2!1sen!2sus!4v1552732389732" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="contact-wrapper">
									<h3 class="mb-30">Interested in discussing?</h3>

									<!-- Contact Form -->
									<form id="contact_form" name="contact_form" class="form-transparent" action="index/email" method="post">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="form_name">Name <small>*</small></label>
													<input id="form_name" name="name" class="form-control" type="text" placeholder="Enter Name" required="">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="form_email">Email <small>*</small></label>
													<input id="form_email" name="email" class="form-control required email" type="email" placeholder="Enter Email">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="form_name">Subject <small>*</small></label>
													<input id="form_subject" name="subject" class="form-control required" type="text" placeholder="Enter Subject">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="form_phone">Phone</label>
													<input id="form_phone" name="phone" class="form-control" type="text" placeholder="Enter Phone">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="form_name">Message</label>
											<textarea id="form_message" name="message" class="form-control required" rows="5" placeholder="Enter Message"></textarea>
										</div>
										<div class="form-group">
											<input id="form_botcheck" name="botcheck" class="form-control" type="hidden" value="" />
											<button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5" data-loading-text="Please wait...">Send your message</button>
											<button type="reset" class="btn btn-default btn-flat btn-theme-colored">Reset</button>
										</div>
									</form>

								</div>
							</div>

						</div>
					</div>
				</div>
			</section>



		</div>

		<!-- Footer -->
		<footer id="footer" class="footer pb-0" data-bg-img="<?php echo base_url('images/footer-bg.png')?>" data-bg-color="#25272e">
			<div class="container pb-20">
				<div class="row multi-row-clearfix">
					<div class="col-sm-6 col-md-5">
						<div class="widget dark"> <img alt="" src="<?php echo base_url('images/logo.png')?>">
							<p class="font-12 mt-20 mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">About Company</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="overview">Overview</a></li>
								<li><a href="team">Our Team</a></li>
								<li><a href="vision">Our Vision</a></li>
								<li><a href="services">Services</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Links</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="register">Register</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Contact</h5>
							<ul class="list-border">
								<li><a href="#">+91 4931 297800</a></li>
								<li><a href="#">info@pravasco.com</a></li>
								<li><a href="#" class="lineheight-20">Pravaso Pvt. Ltd, V K Road, Nilambur, Malappuram Dist, Kerala, 679 329</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-theme-colored p-15">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="text-white font-11 m-0">Copyright &copy;2019 PRAVASCO. All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<p class="text-white font-11 m-0 pull-right">Design & Developed By <a href="http://psybotechnologies.com" target="_blank"><img src="images/cloudbery.png"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.revolution.min.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>
	<!-- Google Map Javascript Codes -->
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyAYWE4mHmR9GyPsHSOVZrSCOOljk8DU9B4"></script>
	<script src="<?php echo base_url('js/google-map-init.js')?>"></script>


    <!-- Contact Form Validation-->
    <script type="text/javascript">
        $("#contact_form").validate({
            submitHandler: function(form) {
                var form_btn = $(form).find('button[type="submit"]');
                var form_result_div = '#form-result';
                $(form_result_div).remove();
                form_btn.before(
                    '<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>'
                );
                var form_btn_old_msg = form_btn();
                form_btn(form_btn.prop('disabled', true).data("loading-text"));
                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == 'true') {
                            $(form).find('.form-control').val('');
                        }
                        form_btn.prop('disabled', false)(form_btn_old_msg);
                        $(form_result_div)(data.message).fadeIn('slow');
                        setTimeout(function() {
                            $(form_result_div).fadeOut('slow')
                        }, 6000);
                    }
                });
            }
        });

    </script>

</body>

</html>
