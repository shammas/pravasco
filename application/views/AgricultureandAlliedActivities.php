<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- Page Title -->
	<title>PRAVASCO | Agriculture Farming and Allied Activities</title>
	<!-- Favicon and Touch Icons -->
	<link href="<?php echo base_url('images/favicon.png" rel="shortcut icon')?>" type="image/png">

	<!-- Stylesheet -->
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/jquery-ui.min.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/animate.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('css/css-plugin-collections.css')?>" rel="stylesheet" />

	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo base_url('css/menuzord-boxed.css')?>" rel="stylesheet" />
	<!-- CSS | Main style file -->
	<link href="<?php echo base_url('css/style-main.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="<?php echo base_url('css/preloader.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="<?php echo base_url('css/custom-bootstrap-margin-padding.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet" type="text/css">
	<!-- CSS | Theme Color -->
	<link href="<?php echo base_url('css/color.css')?>" rel="stylesheet" type="text/css">
	<!-- Revolution Slider 5.x CSS settings -->
	<link href="<?php echo base_url('js/revolution-slider/css/settings.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/layers.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('js/revolution-slider/css/navigation.css')?>" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">
	<div class="body-overlay"></div>
	<div id="wrapper" class="clearfix">
		<!-- preloader -->
		<div id="preloader">
			<div id="spinner">
				<div class="preloader-dot-loading">
					<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
				</div>
			</div>
			<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
		</div>
		<!-- Header -->
		<header class="header">
			<div class="header-top bg-theme-colored sm-text-center">
		    	<div class="container">
	        		<div class="row">
		          		<div class="col-md-9">
		            		<div class="widget no-border m-0">
		              			<ul class="list-inline sm-pull-none sm-text-center mt-5">
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="#">+91 4931 297800</a> </li>
					                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="#">info@pravasco.com</a> </li>
		              			</ul>
		            		</div>
		          		</div>
	          			<div class="col-md-3">
		            		<div class="widget no-border m-0">
		              			<ul class="styled-icons pull-right icon-sm sm-text-center">
					                <li><a href="#" class="text-white"><i class="fa fa-facebook"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-twitter"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-google-plus"></i></a></li>
					                <li><a href="#" class="text-white"><i class="fa fa-linkedin"></i></a></li>
		              			</ul>
		            		</div>
		          		</div>
		        	</div>
		      	</div>
		    </div>
			<div class="header-nav">
				<div class="header-nav-wrapper navbar-scrolltofixed bg-lightest">
					<div class="container">
						<nav id="menuzord-right" class="menuzord orange bg-lightest">
							<a class="menuzord-brand" href="javascript:void(0)"><img src="images/logo.png" alt=""></a>
							<ul class="menuzord-menu">
								<li><a href="index">Home</a></li>
								<li><a href="#home">About Us</a>
									<ul class="dropdown">
										<li><a href="overview">Overview</a></li>
										<li><a href="team">Our Team</a></li>
										<li><a href="vision">Our Vision</a></li>
									</ul>
								</li>
								<li class="active"><a href="services">Services</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
								<li><a href="contact">Contact Us</a></li>
								<li class="active"><a href="register" target="_blank">Register Now</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<!-- Start main-content -->
		<div class="main-content">
			<!-- Section: inner-header -->
			<section class="inner-header bg-black-222">
				<div class="container pt-10 pb-10">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-sm-8 xs-text-center">
								<h3 class="text-white mt-10">Agriculture Farming and Allied Activities</h3>
							</div>
							<div class="col-sm-4">
								<ol class="breadcrumb white mt-10 text-right xs-text-center">
									<li><a href="#">Home</a></li>
									<li class="active">Our Activities</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- Section: About -->
			<section>
				<div class="container pb-0">
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored text-uppercase mt-0">Agriculture Farming and Allied Activities</h3>
							<p>
								Agriculture is considered as the primary activity of an economy, unfortunately it has been given least priority in our society, and even we have hesitations to present as farmers or agriculturist to the present generation. Growth in agriculture is the development indicator of a country and its human civilization. According to the international definition ‘Agriculture’ it is the cultivation and breeding of animals and plants to provide food, fiber, medicinal plants and other products to sustain and enhance life of a society. In other words agriculture and allied activities leads the society towards sustainable life. 
							</p>
							<p>
								The history of Agriculture in India dates back to Indus Valley Civilization Era and even before that in some parts of Southern India. Today, India ranks second worldwide in farm output. Agriculture and allied sectors like forestry and fisheries accounted for 13.7% of the GDP (gross domestic product) in 2013, about 50% of the workforce. The economic contribution of agriculture to India's GDP is steadily declining with the country's broad-based economic growth. Still, agriculture is demographically the broadest economic sector and plays a significant role in the overall socio-economic fabric of India.
							</p>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="post-thumb thumb">
                				<img src="images/services/Agriculture and Allied Activities.jpg" alt="Pravsco Electrical and Electronics Component Manufacturing" class="img-responsive img-fullwidth">
                			</div>
						</div>
						<div class="col-sm-12 col-md-12">
							<p>
								The agricultural scenario in Kerala is somewhat unique and distinct from many other States in India in terms of land utilization pattern and the cropping pattern. Kerala’s agriculture as a whole is growing in terms of income generation from the mid-eighties. This is mainly due to improvement in yield rates and to a smaller extent, due to shift in cropping pattern to high valued crops. In spite of significant advances in industrial and service sectors, agriculture continues to be the largest provider of employment and livelihood both at the national and state levels. But the following issues are found common in the agriculture sector of Kerala.
							</p>
							<ul>
								<li><b>1. </b>Negative attitude towards agriculture and farming due the white color job syndrome </li>
								<li><b>2. </b>Land shortage and urbanization in local areas</li>
								<li><b>3. </b>Non adoption of technology and innovation instead of conventional approach</li>
								<li><b>4. </b>High charges and shortage of man power</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored mt-50">Scope of the Area</h3>
							<p>
								Based on the Panchayatraj law, agriculture is the primary and core activity of a local self-government. Kerala is the only State in India having a well-defined agriculture system starting from the Grama Panchayats. This system is supposed to be the most effective structure for agricultural extension. In fact, the Department makes a presence in all the Grama Panchayat through Krishi Bhavans. The Krishi Bhavan established at Panchayat level functions as grass root level functional as well as technical facilitator agency to farming community via schemes implemented through local bodies and the Department of Agriculture. And also State has an agriculture policy known as <a href="https://kerala.gov.in/documents/10180/46696/AGRICULTURAL%20DEVELOPMENT%20POLICY%202015" class="text-theme-colored" target="_blank">Kerala State Agricultural Development Policy 2015</a>, encompasses a vision of "ensuring a sizable income from farming along with food, water, livelihood security and modern living amenities to every citizen of the State by maintaining sustainability of the agriculture production system and develop agriculture as a worthwhile occupation capable of assuring a modern living with dignity and social status to farmers so that the future generation is attracted towards farming which must be a charming and challenging enterprise to them through the primary sectors like Animal Husbandry, Dairy, Inland Fisheries along with Crop Husbandry including harvesting and handling".
							</p>
							<p>
								In the above context, a wide scope can be explored by the Gulf returnees by identifying proper land and adopting innovative methods in the sector agriculture. 
							</p>
							<h3 class="text-theme-colored mt-50">Green Army Formation</h3>
							<p>
								The company will form a green army for rectifying the shortage of manpower and labour in the agriculture sector, the members of the green army will get good wages and also the shortage of man power issue will be managed through them. 
							</p>
							<h3 class="text-theme-colored mt-50">Ware house and Distribution Centre</h3>
							<p>
								The Pravasco will set up a centralized ware house cum distribution centre, which can ensure the quality processing of the products and also the easy distribution of the products to open market.
							</p>
						</div>
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored mt-50">Interventions Proposed</h3>
							<p>
								The Pravasco may identify and train the members who are interested in Agriculture and allied activities. The following areas are proposed as the sectors.
							</p>
							<ul class="list theme-colored ml-30">
								<li> Farming – Own and leased land can be used</li>
								<li> Livestock management</li>
								<li> Dairy Farms</li>
								<li> Fisheries</li>
								<li> Food Processing</li>
								<li> Forestry activities</li>
								<li> Horticulture </li>
								<li> Ornamental fish</li>
								<li> Herbal Gardens</li>
								<li>Kitchen Gardening for Safe to eat Vegetables</li>
							</ul>
							<p class="mt-20">
								The modern and conventional practices would be incorporated for the best results and output. The following methods will be adopted for different activities.
							</p>
							<ul  class="list theme-colored ml-30">
								<li>Agro farming</li>
								<li>Aquaponics </li>  
								<li>Integrated Farming</li>
								<li>Precision Farming/Poly or green house methods </li>
								<li>Vertical Farming</li>
								<li>Organic farming</li>
								<li>Gro bag farming</li>
							</ul>
							<p class="mt-20">
								The approach would be different and various technological and innovative practices can be adopted such as
							</p>
							<ul  class="list theme-colored ml-30">
								<li>Mechanization for farming</li>
								<li>Technology adoption </li>
								<li>Use of high yield variants</li>
								<li>Adoption of best practices to use minimum land</li>
								<li>Tie with govt dept to use the unused land in their purview </li>
								<li>Collaboration with government departments for technical and financial suport </li>
								<li> Loan Schemes from Financial Institutions.</li>
							</ul>
						</div>
						<div class="col-sm-12 col-md-6">
							<h3 class="text-theme-colored mt-50">Customized Projects</h3>
							<p>
								Company will also support the individual or organisations for any kind of customized electrical/electronic projects as demanded.
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<blockquote class="gray theme-colored">
			                	<p>
			                		In this project Pravasco aims not only the rehabilitation of NRK returnees but also support the productive sector of the State and lead the society towards the sustainable life. It is also envisage to promote organic farming, especially for vegetables and make Nilambur and surroundings self-sufficient in ‘Safe to Eat’ food products. 
			                	</p>
			              	</blockquote>
			            </div>
					</div>
				</div>
			</section>

			<!-- divider: Emergency Services -->
			<section>
				<div class="container">
					<div class="section-content text-center">
						<div class="row">
							<div class="col-md-12">
								<h3 class="mt-0">We can help us</h3>
								<h2>Just call at <span class="text-theme-colored">+91 4931 297800</span> to make a reservation</h2>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- end main-content -->

		<!-- Footer -->
		<footer id="footer" class="footer pb-0" data-bg-img="<?php echo base_url('images/footer-bg.png')?>" data-bg-color="#25272e">
			<div class="container pb-20">
				<div class="row multi-row-clearfix">
					<div class="col-sm-6 col-md-5">
						<div class="widget dark"> <img alt="" src="<?php echo base_url('images/logo.png')?>">
							<p class="font-12 mt-20 mb-10">
								The PRAVASCO is a company formed under Ministry of corporate affairs, Government of India. Pravasco Pvt. Ltd. is an initiative to support, rehabilitate and to create an endless platform to the NRK returnee in their home town. Each sector mentioned in our service section will be functioning as different business units (Bus) of the Company. Each business units will have separate office and activities. The registered individuals, work force, entrepreneurs will be created in each sector with a specific action plan. Heads of each BU will report to the Director Board of the Company.
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">About Company</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="overview">Overview</a></li>
								<li><a href="team">Our Team</a></li>
								<li><a href="vision">Our Vision</a></li>
								<li><a href="services">Services</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-2">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Links</h5>
							<ul class="list-border list theme-colored angle-double-right">
								<li><a href="register">Register</a></li>
								<li><a href="news">News Release</a></li>
								<li><a href="media">Media Centre</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom">Quick Contact</h5>
							<ul class="list-border">
								<li><a href="#">+91 4931 297800</a></li>
								<li><a href="#">info@pravasco.com</a></li>
								<li><a href="#" class="lineheight-20">Pravaso Pvt. Ltd, V K Road, Nilambur, Malappuram Dist, Kerala, 679 329</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-theme-colored p-15">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p class="text-white font-11 m-0">Copyright &copy;2019 PRAVASCO. All Rights Reserved</p>
						</div>
						<div class="col-md-6">
							<p class="text-white font-11 m-0 pull-right">Design & Developed By <a href="http://psybotechnologies.com" target="_blank"><img src="images/cloudbery.png"></a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
        <?php
        if (isset($document) and $document != false) { ?>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
            <a href="<?= $document->url . $document->file_name;?>" download class="fixed-request" title="Download Now">Share Investment Application</a>
        <?php }
        ?>
	</div>
	<!-- end wrapper -->

	<!-- JS | Custom script for all pages -->
	<script src="<?php echo base_url('js/jquery-2.2.0.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('js/jquery-plugin-collection.js')?>"></script>
	<!-- Revolution Slider 5.x SCRIPTS -->
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.tools.min.js')?>"></script>
	<script src="<?php echo base_url('js/revolution-slider/js/jquery.themepunch.revolution.min.js')?>"></script>
	<script src="<?php echo base_url('js/custom.js')?>"></script>

</body>

</html>
