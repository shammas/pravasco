<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Nims Hospital Wandoor. RunBy: Noor Institute of Medical Specialities(P) LTD." />
<meta name="keywords" content="Nims Hospital Wandoor, Noor Institute of Medical Specialities(P) LTD, Hospital in Wandoor, Hospital in Malappuram, Multi Speciality, 24 Hours Lab & Pharmacy in Wandoor, Best hospital in Malappuram" />
<meta name="author" content="Psybo Technologies" />
<!-- Page Title -->
<title>Home | NIMS Hospital</title>
<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url();?>images/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url();?>images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url();?>images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url();?>images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url();?>images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
<!-- Stylesheet -->
<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="<?php echo base_url();?>css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url();?>css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css">
<!-- Revolution Slider 5.x CSS settings -->
<link  href="<?php echo base_url();?>js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo base_url();?>js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo base_url();?>js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>css/colors/theme-skin-blue.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="<?php echo base_url();?>js/jquery-2.2.0.min.js"></script>
<script src="<?php echo base_url();?>js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?php echo base_url();?>js/jquery-plugin-collection.js"></script>
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?php echo base_url();?>js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url();?>js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
</head>

<body class="has-side-panel side-panel-right fullwidth-page side-push-panel">

<div id="wrapper" class="clearfix">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <img src="<?php echo base_url();?>images/preloaders/1.gif" alt="">
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Skip Loading</div>
  </div>

  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-colored sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="widget no-border m-0">
              <ul class="social-icons icon-dark icon-theme-colored icon-sm sm-text-center">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-9">
            <div class="widget no-border m-0">
              <ul class="list-inline pull-right flip sm-pull-none sm-text-center mt-5">
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-white"></i> <a class="text-white" href="#">04931-249827</a> </li>
                <li class="text-white m-0 pl-10 pr-10"> <i class="fa fa-clock-o text-white"></i> 24/7-Service </li>
                <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-white"></i> <a class="text-white" href="#">info@nimshospital.in</a> </li>
                <li class="sm-display-block mt-sm-10 mb-sm-10">
                  <!-- Modal: Appointment Starts -->
                  <a class="bg-light p-5 text-theme-colored font-11 pl-10 pr-10 btn-make" data-toggle="modal" data-target="#myBooking" href="#">Make an Appointment</a>
                  <!-- Modal: Appointment End -->
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
        <div class="container">
          <nav id="menuzord-right" class="menuzord blue bg-white">
            <a class="menuzord-brand pull-left flip" href="javascript:void(0)">
              <img src="<?php echo base_url();?>images/logo.png" alt="">
            </a>
            <ul class="menuzord-menu">
              <li class="<?php echo ($current == 'home' ? 'active' :'')?>"><a href="<?php echo base_url('home');?>">Home</a></li>
              <li class="<?php echo ($current == 'about' ? 'active' :'')?>"><a href="<?php echo base_url('about');?>">About us</a></li>
              <li class="<?php echo ($current == 'departments' ? 'active' :'')?>"><a href="<?php echo base_url('departments');?>">We Expert</a></li>
              <li class="<?php echo ($current == 'services' ? 'active' :'')?>"><a href="<?php echo base_url('services');?>">Our Services</a></li>
              <li class="<?php echo ($current == 'doctors' ? 'active' :'')?>"><a href="<?php echo base_url('doctors');?>">Our Doctors</a></li>
              <li class="<?php echo ($current == 'facilities' ? 'active' :'')?>"><a href="<?php echo base_url('facilities');?>">We Facilitate</a></li>
              <li class="<?php echo ($current == 'updates' ? 'active' :'')?>"><a href="<?php echo base_url('updates');?>">Updates</a></li>
              <li class="<?php echo ($current == 'careers' ? 'active' :'')?>"><a href="<?php echo base_url('careers');?>">Career</a></li>
              <li class="<?php echo ($current == 'moments' ? 'active' :'')?>"><a href="<?php echo base_url('moments');?>">Our Moments</a></li>
              <li class="<?php echo ($current == 'contact' ? 'active' :'')?>"><a href="<?php echo base_url('contact');?>">Contact Us</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>