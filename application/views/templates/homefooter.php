  <!-- Footer -->
  <footer id="footer" class="footer p-0 bg-black-111">
    <div class="container pb-20">
      <div class="row mb-50">
        <div class="footer-box-wrapper equal-height">
          <div class="col-sm-4 footer-box-one pr-0 pl-sm-0">
            <div class="footer-box icon-box media"> <a href="#" class="media-left pull-left mr-30 mr-sm-15"><i class=" icon-ambulance14 text-white"></i></a>
              <div class="media-body">
                <h4 class="media-heading heading title">24 Hours Service</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                <a href="#"><i class="fa fa-arrow-circle-right font-14"></i></a></p>
              </div>
            </div>
          </div>
          <div class="col-sm-4 footer-box-two pl-0 pr-0">
            <div class="footer-box icon-box media"> <a href="#" class="media-left pull-left mr-30 mr-sm-15"><i class="fa fa-comments-o text-white"></i></a>
              <div class="media-body">
                <h4 class="media-heading heading title">Online Help</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                <a href="#"><i class="fa fa-arrow-circle-right font-14"></i></a></p>
              </div>
            </div>
          </div>
          <div class="col-sm-4 footer-box-three pl-0 pr-sm-0">
            <div class="footer-box icon-box media"> <a href="#" class="media-left pull-left mr-30 mr-sm-15"><i class=" fa fa-credit-card text-white"></i></a>
              <div class="media-body">
                <h4 class="media-heading heading title">Online Payment</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                <a href="#"><i class="fa fa-arrow-circle-right font-14"></i></a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row multi-row-clearfix">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="images/logo.png">
            <p class="font-12 mt-20 mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
            <a class="text-gray font-12" href="#"><i class="fa fa-angle-double-right text-theme-colored"></i> Read more</a>
            <ul class="social-icons icon-dark mt-20">
              <li><a href="#" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" data-bg-color="#05A7E3"><i class="fa fa-skype"></i></a></li>
              <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" data-bg-color="#C22E2A"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Links</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="departments.html">Our Departments</a></li>
              <li><a href="doctors.html">Our Doctors</a></li>
              <li><a href="facilities.html">Our Facilities</a></li>
              <li><a href="moments.html">Media Center</a></li>
              <li><a href="policy.html">Hospital Protection Act</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Latest News</h5>
            <div class="latest-posts">
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="single-updates.html">Latest News One here</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2015</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="single-updates.html">Latest News One here</a></h5>
                  <p class="post-date mb-0 font-12">Mar 18, 2015</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="single-updates.html">Latest News One here</a></h5>
                  <p class="post-date mb-0 font-12">Mar 30, 2015</p>
                </div>
              </article>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Contact</h5>
            <ul class="list list-border">
              <li><a href="#">04931-249827 / 249828 / 249829</a></li>
              <li><a href="#">+9194 0004 9828</a></li>
              <li><a href="#">info@nimshospital.in</a></li>
              <li><a href="#" class="lineheight-20">WANDOOR Kalikavu Road, <br />Vaniyambalam Post, PIN: 679 339, <br />Malappuram, KERALA</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-nav bg-black-222 p-20">
      <div class="container pt-20 pb-0">
        <div class="row">
          <div class="col-md-12">
            <p class="font-11 pull-left mb-sm-15 sm-text-center sm-pull-none">Copyright &copy;<span id="year"></span> NIMS. Made with <span class="hrt">&hearts;</span>
             by <a href="http://psybotechnologies.com/" target="_blank">Psybo Technologies.</a></p>
            <div class="widget m-0">
              <ul class="font-11 pull-right text-right list-inline sm-text-center sm-pull-none">
                <li><a href="index..html">Home</a></li>
                <li>/</li> 
                <li><a href="careers.html">Careers</a></li>
                <li>/</li> 
                <li><a href="about.html">About Us</a></li>
                <li>/</li> 
                <li><a href="moments.html">Our Moments</a></li>
                <li>/</li> 
                <li><a href="contact.html">Contact</a></li>
                <li>/</li> 
              </ul>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Modal -->
<div class="modal fade" id="myBooking" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content p-30 pt-10">
      <h3 class="text-center text-theme-colored mb-20">Book Now</h3>
      <form id="booking_form" name="job_apply_form" action="#" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Name <small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Contact Number<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_email">Email <small>*</small></label>
              <input id="form_email" name="form_email" class="form-control required email" type="email" placeholder="Enter Email">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Place<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Date<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Time<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">               
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_sex">Sex <small>*</small></label>
              <select id="form_sex" name="form_sex" class="form-control required">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Your Age<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_post">Select Your Department <small>*</small></label>
              <select id="form_post" name="form_post" class="form-control required">
                <option value="">Department Name</option>
                <option value="">Department Name</option>
                <option value="">Department Name</option>
                <option value="">Department Name</option>
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_post">Select Your Doctor <small>*</small></label>
              <select id="form_post" name="form_post" class="form-control required">
                <option value="">Doctor Name</option>
                <option value="">Doctor Name</option>
                <option value="">Doctor Name</option>
                <option value="">Doctor Name</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
          <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Book Now</button>
        </div>
      </form>
      <!-- Job Form Validation-->
      <script type="text/javascript">
        $("#job_apply_form").validate({
          submitHandler: function(form) {
            var form_btn = $(form).find('button[type="submit"]');
            var form_result_div = '#form-result';
            $(form_result_div).remove();
            form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
            var form_btn_old_msg = form_btn.html();
            form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
            $(form).ajaxSubmit({
              dataType:  'json',
              success: function(data) {
                if( data.status == 'true' ) {
                  $(form).find('.form-control').val('');
                }
                form_btn.prop('disabled', false).html(form_btn_old_msg);
                $(form_result_div).html(data.message).fadeIn('slow');
                setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
              }
            });
          }
        });
      </script>
    </div>
  </div>
</div>

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
<!-- end .rev_slider_wrapper -->
<script>
  $(document).ready(function(e) {
    var revapi = $(".rev_slider").revolution({
      sliderType:"standard",
      sliderLayout: "auto",
      dottedOverlay: "none",
      delay: 5000,
      navigation: {
          keyboardNavigation: "off",
          keyboard_direction: "horizontal",
          mouseScrollNavigation: "off",
          onHoverStop: "off",
          touch: {
              touchenabled: "on",
              swipe_threshold: 75,
              swipe_min_touches: 1,
              swipe_direction: "horizontal",
              drag_block_vertical: false
          },
          arrows: {
              style: "gyges",
              enable: true,
              hide_onmobile: false,
              hide_onleave: true,
              hide_delay: 200,
              hide_delay_mobile: 1200,
              tmp: '',
              left: {
                  h_align: "left",
                  v_align: "center",
                  h_offset: 0,
                  v_offset: 0
              },
              right: {
                  h_align: "right",
                  v_align: "center",
                  h_offset: 0,
                  v_offset: 0
              }
          },
            bullets: {
            enable: true,
            hide_onmobile: true,
            hide_under: 800,
            style: "hebe",
            hide_onleave: false,
            direction: "horizontal",
            h_align: "center",
            v_align: "bottom",
            h_offset: 0,
            v_offset: 30,
            space: 5,
            tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
        }
      },
      responsiveLevels: [1240, 1024, 778],
      visibilityLevels: [1240, 1024, 778],
      gridwidth: [1170, 1024, 778, 480],
      gridheight: [655, 768, 960, 720],
      lazyType: "none",
      parallax: {
          origo: "slidercenter",
          speed: 1000,
          levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
          type: "scroll"
      },
      shadow: 0,
      spinner: "off",
      stopLoop: "on",
      stopAfterLoops: 0,
      stopAtSlide: -1,
      shuffle: "off",
      autoHeight: "off",
      fullScreenAutoWidth: "off",
      fullScreenAlignForce: "off",
      fullScreenOffsetContainer: "",
      fullScreenOffset: "0",
      hideThumbsOnMobile: "off",
      hideSliderAtLimit: 0,
      hideCaptionAtLimit: 0,
      hideAllCaptionAtLilmit: 0,
      debugMode: false,
      fallbacks: {
          simplifyAll: "off",
          nextSlideOnWindowFocus: "off",
          disableFocusListener: false,
      }
    });
  });
</script>
<!-- Slider Revolution Ends -->
<!-- year scrpt -->
<script type="text/javascript">
    n =  new Date();
    y = n.getFullYear();
    document.getElementById("year").innerHTML = y;
</script>

</body>
</html>