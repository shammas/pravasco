  <!-- Footer -->
  <footer id="footer" class="footer p-0 bg-black-111">
    <div class="container pb-20">
      <div class="row pt-50 multi-row-clearfix">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="<?php echo base_url();?>images/logo.png">
            <p class="font-12 mt-20 mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
            <a class="text-gray font-12" href="#"><i class="fa fa-angle-double-right text-theme-colored"></i> Read more</a>
            <ul class="social-icons icon-dark mt-20">
              <li><a href="#" data-bg-color="#3B5998"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" data-bg-color="#02B0E8"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" data-bg-color="#05A7E3"><i class="fa fa-skype"></i></a></li>
              <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" data-bg-color="#C22E2A"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Links</h5>
            <ul class="list-border list theme-colored angle-double-right">
              <li><a href="departments">Our Departments</a></li>
              <li><a href="doctors">Our Doctors</a></li>
              <li><a href="facilities">Our Facilities</a></li>
              <li><a href="moments">Media Center</a></li>
              <li><a href="policy">Hospital Protection Act</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Latest News</h5>
            <div class="latest-posts">
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="single-updates.html">Latest News One here</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2015</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="single-updates.html">Latest News One here</a></h5>
                  <p class="post-date mb-0 font-12">Mar 18, 2015</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="single-updates.html">Latest News One here</a></h5>
                  <p class="post-date mb-0 font-12">Mar 30, 2015</p>
                </div>
              </article>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Quick Contact</h5>
            <ul class="list list-border">
              <li><a href="#">04931-249827 / 249828 / 249829</a></li>
              <li><a href="#">+9194 0004 9828</a></li>
              <li><a href="#">info@nimshospital.in</a></li>
              <li><a href="#" class="lineheight-20">WANDOOR Kalikavu Road, <br />Vaniyambalam Post, PIN: 679 339, <br />Malappuram, KERALA</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-nav bg-black-222 p-20">
      <div class="container pt-20 pb-0">
        <div class="row">
          <div class="col-md-12">
            <p class="font-11 pull-left mb-sm-15 sm-text-center sm-pull-none">Copyright &copy;<span id="year"></span> NIMS. Made with <span class="hrt">&hearts;</span>
             by <a href="http://psybotechnologies.com/" target="_blank">Psybo Technologies.</a></p>
            <div class="widget m-0">
              <ul class="font-11 pull-right text-right list-inline sm-text-center sm-pull-none">
                <li><a href="index">Home</a></li>
                <li>/</li> 
                <li><a href="careers">Careers</a></li>
                <li>/</li> 
                <li><a href="about">About Us</a></li>
                <li>/</li> 
                <li><a href="moments">Our Moments</a></li>
                <li>/</li> 
                <li><a href="contact">Contact</a></li>
                <li>/</li> 
              </ul>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Modal -->
<div class="modal fade" id="myBooking" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content p-30 pt-10">
      <h3 class="text-center text-theme-colored mb-20">Book Now</h3>
      <form id="booking_form" name="job_apply_form" action="#" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Name <small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Contact Number<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_email">Email <small>*</small></label>
              <input id="form_email" name="form_email" class="form-control required email" type="email" placeholder="Enter Email">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Place<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Date<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Time<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">               
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_sex">Sex <small>*</small></label>
              <select id="form_sex" name="form_sex" class="form-control required">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_name">Your Age<small>*</small></label>
              <input id="form_name" name="form_name" type="text" placeholder="Enter Name" required="" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_post">Select Your Department <small>*</small></label>
              <select id="form_post" name="form_post" class="form-control required">
                <option value="">Department Name</option>
                <option value="">Department Name</option>
                <option value="">Department Name</option>
                <option value="">Department Name</option>
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="form_post">Select Your Doctor <small>*</small></label>
              <select id="form_post" name="form_post" class="form-control required">
                <option value="">Doctor Name</option>
                <option value="">Doctor Name</option>
                <option value="">Doctor Name</option>
                <option value="">Doctor Name</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
          <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">Book Now</button>
        </div>
      </form>
      <!-- Job Form Validation-->
      <script type="text/javascript">
        $("#job_apply_form").validate({
          submitHandler: function(form) {
            var form_btn = $(form).find('button[type="submit"]');
            var form_result_div = '#form-result';
            $(form_result_div).remove();
            form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
            var form_btn_old_msg = form_btn.html();
            form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
            $(form).ajaxSubmit({
              dataType:  'json',
              success: function(data) {
                if( data.status == 'true' ) {
                  $(form).find('.form-control').val('');
                }
                form_btn.prop('disabled', false).html(form_btn_old_msg);
                $(form_result_div).html(data.message).fadeIn('slow');
                setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
              }
            });
          }
        });
      </script>
    </div>
  </div>
</div>

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="<?php echo base_url();?>js/custom.js"></script>
<!-- year scrpt -->
<script type="text/javascript">
    n =  new Date();
    y = n.getFullYear();
    document.getElementById("year").innerHTML = y;
</script>
</body>
</html>